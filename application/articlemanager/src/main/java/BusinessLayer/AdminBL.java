/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package BusinessLayer;

/**
 *
 * @author Daniel G
 */
public class AdminBL {
    private Integer PersonId;

    public AdminBL() {
    }

    public AdminBL(Integer PersonId) {
        this.PersonId = PersonId;
    }
    
    public void setPersonId(Integer PersonId) {
        this.PersonId = PersonId;
    }

    public Integer getPersonId() {
        return PersonId;
    }
    
    
}
