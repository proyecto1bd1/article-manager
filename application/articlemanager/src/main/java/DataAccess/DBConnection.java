/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DataAccess;
import java.sql.*;
import com.sun.jdi.connect.spi.Connection;

/**
 *
 * @author Daniel G
 */
public class DBConnection {
    private static final String URL = "jdbc:oracle:thin:@localhost:1521:dbprueba";
    private static final String USER = "P1DB";
    private static final String PASS = "shoesarebesticecream";
     
    public static Connection getCon() {
            try
            {
            DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
            java.sql.Connection connection = DriverManager.getConnection(URL, USER, PASS);
            return (Connection) connection;
            }
            catch(SQLException e)
            {
            System.out.println("Error: Unable to load driver");
            return null;
            }            
    } 
}
