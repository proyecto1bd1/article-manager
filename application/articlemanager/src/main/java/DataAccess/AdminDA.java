/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DataAccess;

/**
 *
 * @author Daniel G
 */
public class AdminDA {
    private Integer PersonID;

    public AdminDA() {
    }

    public AdminDA(Integer PersonID) {
        this.PersonID = PersonID;
    }

    public void setPersonID(Integer PersonID) {
        this.PersonID = PersonID;
    }

    public Integer getPersonID() {
        return PersonID;
    }

}
