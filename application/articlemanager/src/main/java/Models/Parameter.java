/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

/**
 *
 * @author Daniel G
 */
public class Parameter {
    private Integer ParameterId;
    private String Name;
    private String Description;
    private String Value;
    private Integer UniversityId;

    public Parameter() {
    }
    
    

    public Parameter(Integer ParameterId, String Name, String Description, String Value, Integer UniversityId) {
        this.ParameterId = ParameterId;
        this.Name = Name;
        this.Description = Description;
        this.Value = Value;
        this.UniversityId = UniversityId;
    }

    public Parameter(String Name, String Description, String Value, Integer UniversityId) {
        this.Name = Name;
        this.Description = Description;
        this.Value = Value;
        this.UniversityId = UniversityId;
    }

    public void setParameterId(Integer ParameterId) {
        this.ParameterId = ParameterId;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public void setValue(String Value) {
        this.Value = Value;
    }

    public void setUniversityId(Integer UniversityId) {
        this.UniversityId = UniversityId;
    }

    public Integer getParameterId() {
        return ParameterId;
    }

    public String getName() {
        return Name;
    }

    public String getDescription() {
        return Description;
    }

    public String getValue() {
        return Value;
    }

    public Integer getUniversityId() {
        return UniversityId;
    }
}
