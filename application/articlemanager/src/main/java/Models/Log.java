/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

/**
 *
 * @author Daniel G
 */
public class Log {
    private Integer LogId;
    private String FieldName;
    private String TableName;
    private String CurrentValue;
    private String PreviousValue;
    private Integer ArticleId;

    public Log() {
    }

    
    
    public Log(Integer LogId, String FieldName, String TableName, String CurrentValue, String PreviousValue, Integer ArticleId) {
        this.LogId = LogId;
        this.FieldName = FieldName;
        this.TableName = TableName;
        this.CurrentValue = CurrentValue;
        this.PreviousValue = PreviousValue;
        this.ArticleId = ArticleId;
    }

    public Log(String FieldName, String TableName, String CurrentValue, String PreviousValue, Integer ArticleId) {
        this.FieldName = FieldName;
        this.TableName = TableName;
        this.CurrentValue = CurrentValue;
        this.PreviousValue = PreviousValue;
        this.ArticleId = ArticleId;
    }

    public void setLogId(Integer LogId) {
        this.LogId = LogId;
    }

    public void setFieldName(String FieldName) {
        this.FieldName = FieldName;
    }

    public void setTableName(String TableName) {
        this.TableName = TableName;
    }

    public void setCurrentValue(String CurrentValue) {
        this.CurrentValue = CurrentValue;
    }

    public void setPreviousValue(String PreviousValue) {
        this.PreviousValue = PreviousValue;
    }

    public void setArticleId(Integer ArticleId) {
        this.ArticleId = ArticleId;
    }

    public Integer getLogId() {
        return LogId;
    }

    public String getFieldName() {
        return FieldName;
    }

    public String getTableName() {
        return TableName;
    }

    public String getCurrentValue() {
        return CurrentValue;
    }

    public String getPreviousValue() {
        return PreviousValue;
    }

    public Integer getArticleId() {
        return ArticleId;
    }
}
