/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

/**
 *
 * @author Daniel G
 */
public class IdType {
    
    private Integer TypeId;
    private String Description;
    private String Mask;

    public IdType() {
    }
    
    

    public IdType(Integer TypeId, String Description, String Mask) {
        this.TypeId = TypeId;
        this.Description = Description;
        this.Mask = Mask;
    }

    public IdType(String Description, String Mask) {
        this.Description = Description;
        this.Mask = Mask;
    }

    public Integer getTypeId() {
        return TypeId;
    }

    public String getDescription() {
        return Description;
    }

    public String getMask() {
        return Mask;
    }

    public void setTypeId(Integer TypeId) {
        this.TypeId = TypeId;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public void setMask(String Mask) {
        this.Mask = Mask;
    }
    
    
            
}
