/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

/**
 *
 * @author Daniel G
 */
public class PersonXNewspaper {
    private Integer NewsPaperId;
    private Integer PersonId;

    public PersonXNewspaper() {
    }
    
    

    public PersonXNewspaper(Integer NewsPaperId, Integer PersonId) {
        this.NewsPaperId = NewsPaperId;
        this.PersonId = PersonId;
    }

    public void setNewsPaperId(Integer NewsPaperId) {
        this.NewsPaperId = NewsPaperId;
    }

    public void setPersonId(Integer PersonId) {
        this.PersonId = PersonId;
    }

    public Integer getNewsPaperId() {
        return NewsPaperId;
    }

    public Integer getPersonId() {
        return PersonId;
    }
    
}
