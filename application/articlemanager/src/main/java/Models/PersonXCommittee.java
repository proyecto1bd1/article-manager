/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

/**
 *
 * @author Daniel G
 */
public class PersonXCommittee {
    private Integer PersonId;
    private Integer CommitteeId;

    public PersonXCommittee() {
    }
    
    

    public PersonXCommittee(Integer PersonId, Integer CommitteeId) {
        this.PersonId = PersonId;
        this.CommitteeId = CommitteeId;
    }

    public void setPersonId(Integer PersonId) {
        this.PersonId = PersonId;
    }

    public void setCommitteeId(Integer CommitteeId) {
        this.CommitteeId = CommitteeId;
    }

    public Integer getPersonId() {
        return PersonId;
    }

    public Integer getCommitteeId() {
        return CommitteeId;
    }
    
    
}
