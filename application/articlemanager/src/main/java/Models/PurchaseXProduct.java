/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

/**
 *
 * @author Daniel G
 */
public class PurchaseXProduct {
    private Integer PurchaseId;
    private Integer ProductId;

    public PurchaseXProduct() {
    }
    
    

    public PurchaseXProduct(Integer PurchaseId, Integer ProductId) {
        this.PurchaseId = PurchaseId;
        this.ProductId = ProductId;
    }

    public void setPurchaseId(Integer PurchaseId) {
        this.PurchaseId = PurchaseId;
    }

    public void setProductId(Integer ProductId) {
        this.ProductId = ProductId;
    }

    public Integer getPurchaseId() {
        return PurchaseId;
    }

    public Integer getProductId() {
        return ProductId;
    }
}

