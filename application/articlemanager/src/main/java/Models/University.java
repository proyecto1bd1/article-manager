/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

/**
 *
 * @author Daniel G
 */
public class University {
    private Integer UniversityId;
    private String UniversityName;
    private Integer NewsPaperId;
    private Integer DistrictId;

    public University() {
    }
    
    

    public University(Integer UniversityId, String UniversityName, Integer NewsPaperId, Integer DistrictId) {
        this.UniversityId = UniversityId;
        this.UniversityName = UniversityName;
        this.NewsPaperId = NewsPaperId;
        this.DistrictId = DistrictId;
    }

    public University(String UniversityName, Integer NewsPaperId, Integer DistrictId) {
        this.UniversityName = UniversityName;
        this.NewsPaperId = NewsPaperId;
        this.DistrictId = DistrictId;
    }

    public void setUniversityId(Integer UniversityId) {
        this.UniversityId = UniversityId;
    }

    public void setUniversityName(String UniversityName) {
        this.UniversityName = UniversityName;
    }

    public void setNewsPaperId(Integer NewsPaperId) {
        this.NewsPaperId = NewsPaperId;
    }

    public void setDistrictId(Integer DistrictId) {
        this.DistrictId = DistrictId;
    }

    public Integer getUniversityId() {
        return UniversityId;
    }

    public String getUniversityName() {
        return UniversityName;
    }

    public Integer getNewsPaperId() {
        return NewsPaperId;
    }

    public Integer getDistrictId() {
        return DistrictId;
    }
}
