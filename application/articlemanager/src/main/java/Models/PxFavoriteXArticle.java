/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

/**
 *
 * @author Daniel G
 */
public class PxFavoriteXArticle {
    private Integer PersonId;
    private Integer ArticleId;
    private boolean deleted;

    public PxFavoriteXArticle() {
    }
    
    

    public PxFavoriteXArticle(Integer PersonId, Integer ArticleId, boolean deleted) {
        this.PersonId = PersonId;
        this.ArticleId = ArticleId;
        this.deleted = deleted;
    }

    public void setPersonId(Integer PersonId) {
        this.PersonId = PersonId;
    }

    public void setArticleId(Integer ArticleId) {
        this.ArticleId = ArticleId;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Integer getPersonId() {
        return PersonId;
    }

    public Integer getArticleId() {
        return ArticleId;
    }

    public boolean isDeleted() {
        return deleted;
    }


}
