/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

/**
 *
 * @author Daniel G
 */
public class Committee {
    private Integer CommitteeId;
    private String Description;

    public Committee() {
    }
   
    public Committee(Integer CommitteeId, String Description) {
        this.CommitteeId = CommitteeId;
        this.Description = Description;
    }

    public Committee(String Description) {
        this.Description = Description;
    }

    public void setCommitteeId(Integer CommitteeId) {
        this.CommitteeId = CommitteeId;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public Integer getCommitteeId() {
        return CommitteeId;
    }

    public String getDescription() {
        return Description;
    }
}
