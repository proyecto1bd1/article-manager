/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

/**
 *
 * @author Daniel G
 */
public class Canton {
    
    private Integer cantonId;
    private String cantonName;
    private Integer provinceId;

    public Canton() {
    }

    public Canton(Integer cantonId, String cantonName, Integer provinceId) {
        this.cantonId = cantonId;
        this.cantonName = cantonName;
        this.provinceId = provinceId;
    }

    public Canton(String cantonName, Integer provinceId) {
        this.cantonName = cantonName;
        this.provinceId = provinceId;
    }
    
    

    public void setCantonId(Integer cantonId) {
        this.cantonId = cantonId;
    }

    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }

    public Integer getCantonId() {
        return cantonId;
    }

    public Integer getProvinceId() {
        return provinceId;
    }
    
   
    public void setCantonName(String cantonName) {
        this.cantonName = cantonName;
    }

    public String getCantonName() {
        return cantonName;
    }   
}
