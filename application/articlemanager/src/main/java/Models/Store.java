/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

/**
 *
 * @author Daniel G
 */
public class Store {
    private Integer StoreId;
    private String Description;
    private Integer NewspaperId;

    public Store() {
    }
    
    

    public Store(Integer StoreId, String Description, Integer NewspaperId) {
        this.StoreId = StoreId;
        this.Description = Description;
        this.NewspaperId = NewspaperId;
    }

    public Store(String Description, Integer NewspaperId) {
        this.Description = Description;
        this.NewspaperId = NewspaperId;
    }

    public void setStoreId(Integer StoreId) {
        this.StoreId = StoreId;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public void setNewspaperId(Integer NewspaperId) {
        this.NewspaperId = NewspaperId;
    }

    public Integer getStoreId() {
        return StoreId;
    }

    public String getDescription() {
        return Description;
    }

    public Integer getNewspaperId() {
        return NewspaperId;
    }
    
    
}
