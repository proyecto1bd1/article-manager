/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

import java.sql.Blob;
import java.util.Calendar;

/**
 *
 * @author Daniel G
 */



public class NewsArticle {
    private Integer ArticleId;
    private String Title;
    private String Text;
    private Calendar PublicationDate;
    private Blob Photo;
    private Integer AuthorId;
    private Integer ArticleTypeId;
    private Integer NewsPaperId;
    private Integer CommitteeIdAprov;

    public NewsArticle() {
    }
    
    

    public NewsArticle(Integer ArticleId, String Title, String Text, Calendar PublicationDate, Blob Photo, Integer AuthorId, Integer ArticleTypeId, Integer NewsPaperId, Integer CommitteeIdAprov) {
        this.ArticleId = ArticleId;
        this.Title = Title;
        this.Text = Text;
        this.PublicationDate = PublicationDate;
        this.Photo = Photo;
        this.AuthorId = AuthorId;
        this.ArticleTypeId = ArticleTypeId;
        this.NewsPaperId = NewsPaperId;
        this.CommitteeIdAprov = CommitteeIdAprov;
    }

    public NewsArticle(String Title, String Text, Calendar PublicationDate, Blob Photo, Integer AuthorId, Integer ArticleTypeId, Integer NewsPaperId, Integer CommitteeIdAprov) {
        this.Title = Title;
        this.Text = Text;
        this.PublicationDate = PublicationDate;
        this.Photo = Photo;
        this.AuthorId = AuthorId;
        this.ArticleTypeId = ArticleTypeId;
        this.NewsPaperId = NewsPaperId;
        this.CommitteeIdAprov = CommitteeIdAprov;
    }

    public void setArticleId(Integer ArticleId) {
        this.ArticleId = ArticleId;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public void setText(String Text) {
        this.Text = Text;
    }

    public void setPublicationDate(Calendar PublicationDate) {
        this.PublicationDate = PublicationDate;
    }

    public void setPhoto(Blob Photo) {
        this.Photo = Photo;
    }

    public void setAuthorId(Integer AuthorId) {
        this.AuthorId = AuthorId;
    }

    public void setArticleTypeId(Integer ArticleTypeId) {
        this.ArticleTypeId = ArticleTypeId;
    }

    public void setNewsPaperId(Integer NewsPaperId) {
        this.NewsPaperId = NewsPaperId;
    }

    public void setCommitteeIdAprov(Integer CommitteeIdAprov) {
        this.CommitteeIdAprov = CommitteeIdAprov;
    }

    public Integer getArticleId() {
        return ArticleId;
    }

    public String getTitle() {
        return Title;
    }

    public String getText() {
        return Text;
    }

    public Calendar getPublicationDate() {
        return PublicationDate;
    }

    public Blob getPhoto() {
        return Photo;
    }

    public Integer getAuthorId() {
        return AuthorId;
    }

    public Integer getArticleTypeId() {
        return ArticleTypeId;
    }

    public Integer getNewsPaperId() {
        return NewsPaperId;
    }

    public Integer getCommitteeIdAprov() {
        return CommitteeIdAprov;
    }
    
    
    
    
}
