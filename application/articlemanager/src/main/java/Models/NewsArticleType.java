/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

/**
 *
 * @author Daniel G
 */
public class NewsArticleType {
    private Integer TypeId;
    private String Description;

    public NewsArticleType() {
    }
    
    

    public NewsArticleType(Integer TypeId, String Description) {
        this.TypeId = TypeId;
        this.Description = Description;
    }

    public NewsArticleType(String Description) {
        this.Description = Description;
    }

    public void setTypeId(Integer TypeId) {
        this.TypeId = TypeId;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public Integer getTypeId() {
        return TypeId;
    }

    public String getDescription() {
        return Description;
    } 
}
