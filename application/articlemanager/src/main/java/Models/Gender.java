/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

/**
 *
 * @author Daniel G
 */

public class Gender {
    
    private Integer genderId;
    private String genderDescript;

    public Gender() {
    }

    public Gender(Integer genderId, String genderDescript) {
        this.genderId = genderId;
        this.genderDescript = genderDescript;
    }

    public Gender(String genderDescript) {
        this.genderDescript = genderDescript;
    }
    
    public void setGenderId(Integer genderId) {
        this.genderId = genderId;
    }

    public void setGenderDescript(String genderDescript) {
        this.genderDescript = genderDescript;
    }

    public Integer getGenderId() {
        return genderId;
    }

    public String getGenderDescript() {
        return genderDescript;
    }   
}