/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

/**
 *
 * @author Daniel G
 */
public class DigitalNewsPaper {
    private Integer NewsPaperId;
    private String NewsPaperName;
    private Integer CommitteeId;

    public DigitalNewsPaper() {
    }
    
    

    public DigitalNewsPaper(Integer NewsPaperId, String NewsPaperName, Integer CommitteeId) {
        this.NewsPaperId = NewsPaperId;
        this.NewsPaperName = NewsPaperName;
        this.CommitteeId = CommitteeId;
    }

    public DigitalNewsPaper(String NewsPaperName, Integer CommitteeId) {
        this.NewsPaperName = NewsPaperName;
        this.CommitteeId = CommitteeId;
    }

    public void setNewsPaperId(Integer NewsPaperId) {
        this.NewsPaperId = NewsPaperId;
    }

    public void setNewsPaperName(String NewsPaperName) {
        this.NewsPaperName = NewsPaperName;
    }

    public void setCommitteeId(Integer CommitteeId) {
        this.CommitteeId = CommitteeId;
    }

    public Integer getNewsPaperId() {
        return NewsPaperId;
    }

    public String getNewsPaperName() {
        return NewsPaperName;
    }

    public Integer getCommitteeId() {
        return CommitteeId;
    }
    
    
}
