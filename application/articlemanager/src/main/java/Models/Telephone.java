/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

/**
 *
 * @author Daniel G
 */
public class Telephone {
    
    private Integer telephoneid;
    private String phoneNumber;
    private Integer personId;

    public Telephone() {
    }

    public Telephone(Integer telephoneid, String phoneNumber, Integer personId) {
        this.telephoneid = telephoneid;
        this.phoneNumber = phoneNumber;
        this.personId = personId;
    }

    public Telephone(String phoneNumber, Integer personId) {
        this.phoneNumber = phoneNumber;
        this.personId = personId;
    }

    public void setTelephoneid(Integer telephoneid) {
        this.telephoneid = telephoneid;
    }

    public void setTelephoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public Integer getTelephoneid() {
        return telephoneid;
    }

    public String getTelephoneNumber() {
        return phoneNumber;
    }

    public Integer getPersonId() {
        return personId;
    }   
}