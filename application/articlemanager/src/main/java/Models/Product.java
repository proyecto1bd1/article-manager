/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

/**
 *
 * @author Daniel G
 */
public class Product {
    private Integer ProductId;
    private String Name;
    private Integer NumberInStock;
    private Integer StoreId;

    public Product() {
    }
    
    

    public Product(Integer ProductId, String Name, Integer NumberInStock, Integer StoreId) {
        this.ProductId = ProductId;
        this.Name = Name;
        this.NumberInStock = NumberInStock;
        this.StoreId = StoreId;
    }

    public Product(String Name, Integer NumberInStock, Integer StoreId) {
        this.Name = Name;
        this.NumberInStock = NumberInStock;
        this.StoreId = StoreId;
    }

    public void setProductId(Integer ProductId) {
        this.ProductId = ProductId;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public void setNumberInStock(Integer NumberInStock) {
        this.NumberInStock = NumberInStock;
    }

    public void setStoreId(Integer StoreId) {
        this.StoreId = StoreId;
    }

    public Integer getProductId() {
        return ProductId;
    }

    public String getName() {
        return Name;
    }

    public Integer getNumberInStock() {
        return NumberInStock;
    }

    public Integer getStoreId() {
        return StoreId;
    }
    
    
}
