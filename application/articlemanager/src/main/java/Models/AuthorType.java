/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

/**
 *
 * @author Daniel G
 */
public class AuthorType {
    private Integer AuthorTypeId;
    private String Description;

    public AuthorType() {
    }

    public AuthorType(Integer AuthorTypeId, String Description) {
        this.AuthorTypeId = AuthorTypeId;
        this.Description = Description;
    }

    public AuthorType(String Description) {
        this.Description = Description;
    }

    public void setAuthorTypeId(Integer AuthorTypeId) {
        this.AuthorTypeId = AuthorTypeId;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public Integer getAuthorTypeId() {
        return AuthorTypeId;
    }

    public String getDescription() {
        return Description;
    }
}
