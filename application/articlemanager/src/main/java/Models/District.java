/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

/**
 *
 * @author Daniel G
 */
public class District {
    
    private Integer districtId;
    private String districtName;
    private Integer cantonId;

    public District() {
    }

    public District(Integer districtId, String districtName, Integer cantonId) {
        this.districtId = districtId;
        this.districtName = districtName;
        this.cantonId = cantonId;
    }

    public District(String districtName, Integer cantonId) {
        this.districtName = districtName;
        this.cantonId = cantonId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setCantonId(Integer cantonId) {
        this.cantonId = cantonId;
    }

    public Integer getCantonId() {
        return cantonId;
    }
}
