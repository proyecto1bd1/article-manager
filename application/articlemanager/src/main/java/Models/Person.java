/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

import java.util.Calendar;

/**
 *
 * @author Daniel G
 */
public class Person {
    private Integer PersonId;
    private Integer IdCard;
    private String Name;
    private String Firstlastname;
    private String Secondlastname;
    private Calendar Birthdate;
    private String LocalAddress;
    private String Password;
    private Integer Districtid;
    private Integer Genderid;
    private Integer typeid;

    public Person() {
    }

    public Person(Integer PersonId, Integer IdCard, String Name, String Firstlastname, String Secondlastname, Calendar Birthdate, String LocalAddress, String Password, Integer Districtid, Integer Genderid, Integer typeid) {
        this.PersonId = PersonId;
        this.IdCard = IdCard;
        this.Name = Name;
        this.Firstlastname = Firstlastname;
        this.Secondlastname = Secondlastname;
        this.Birthdate = Birthdate;
        this.LocalAddress = LocalAddress;
        this.Password = Password;
        this.Districtid = Districtid;
        this.Genderid = Genderid;
        this.typeid = typeid;
    }

    public Person(Integer IdCard, String Name, String Firstlastname, String Secondlastname, Calendar Birthdate, String LocalAddress, String Password, Integer Districtid, Integer Genderid, Integer typeid) {
        this.IdCard = IdCard;
        this.Name = Name;
        this.Firstlastname = Firstlastname;
        this.Secondlastname = Secondlastname;
        this.Birthdate = Birthdate;
        this.LocalAddress = LocalAddress;
        this.Password = Password;
        this.Districtid = Districtid;
        this.Genderid = Genderid;
        this.typeid = typeid;
    }

    public void setPersonId(Integer PersonId) {
        this.PersonId = PersonId;
    }

    public void setIdCard(Integer IdCard) {
        this.IdCard = IdCard;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public void setFirstlastname(String Firstlastname) {
        this.Firstlastname = Firstlastname;
    }

    public void setSecondlastname(String Secondlastname) {
        this.Secondlastname = Secondlastname;
    }

    public void setBirthdate(Calendar Birthdate) {
        this.Birthdate = Birthdate;
    }

    public void setLocalAddress(String LocalAddress) {
        this.LocalAddress = LocalAddress;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public void setDistrictid(Integer Districtid) {
        this.Districtid = Districtid;
    }

    public void setGenderid(Integer Genderid) {
        this.Genderid = Genderid;
    }

    public void setTypeid(Integer typeid) {
        this.typeid = typeid;
    }

    public Integer getPersonId() {
        return PersonId;
    }

    public Integer getIdCard() {
        return IdCard;
    }

    public String getName() {
        return Name;
    }

    public String getFirstlastname() {
        return Firstlastname;
    }

    public String getSecondlastname() {
        return Secondlastname;
    }

    public Calendar getBirthdate() {
        return Birthdate;
    }

    public String getLocalAddress() {
        return LocalAddress;
    }

    public String getPassword() {
        return Password;
    }

    public Integer getDistrictid() {
        return Districtid;
    }

    public Integer getGenderid() {
        return Genderid;
    }

    public Integer getTypeid() {
        return typeid;
    }

}


