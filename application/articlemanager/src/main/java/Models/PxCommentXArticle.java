/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;
/**
 *
 * @author Daniel G
 */
public class PxCommentXArticle {
    private Integer PersonId;
    private Integer ArticleId;
    private String Comments;

    public PxCommentXArticle() {
    }
    
    

    public PxCommentXArticle(Integer PersonId, Integer ArticleId, String Comments) {
        this.PersonId = PersonId;
        this.ArticleId = ArticleId;
        this.Comments = Comments;
    }

    public PxCommentXArticle(Integer ArticleId, String Comments) {
        this.ArticleId = ArticleId;
        this.Comments = Comments;
    }

    public void setPersonId(Integer PersonId) {
        this.PersonId = PersonId;
    }

    public void setArticleId(Integer ArticleId) {
        this.ArticleId = ArticleId;
    }

    public void setComments(String Comments) {
        this.Comments = Comments;
    }

    public Integer getPersonId() {
        return PersonId;
    }

    public Integer getArticleId() {
        return ArticleId;
    }

    public String getComments() {
        return Comments;
    } 
}
