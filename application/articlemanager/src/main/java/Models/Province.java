/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

/**
 *
 * @author Daniel G
 */
public class Province {
    
    private Integer provinceId;
    private String provinceName;
    private Integer countryId;
    
    public Province() {
    }

    public Province(String provinceName, Integer countryId) {
        this.provinceName = provinceName;
        this.countryId = countryId;
    }

    
    public Province(Integer provinceId, String provinceName, Integer countryId) {
        this.provinceId = provinceId;
        this.provinceName = provinceName;
        this.countryId = countryId;
    }

    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }

    public Integer getProvinceId() {
        return provinceId;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public Integer getCountryId() {
        return countryId;
    }   
}
