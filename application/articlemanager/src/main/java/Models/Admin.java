/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

/**
 *
 * @author Daniel G
 */
public class Admin {
    private Integer PersonID;

    public Admin() {
    }

    public Admin(Integer PersonID) {
        this.PersonID = PersonID;
    }

    public void setPersonID(Integer PersonID) {
        this.PersonID = PersonID;
    }

    public Integer getPersonID() {
        return PersonID;
    }
    
    
}
