/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

/**
 *
 * @author Daniel G
 */
public class Email {
    
    private Integer emailId;
    private String emailAddress;
    private Integer PersonId;

    public Email() {
    }

    public Email(Integer emailId, String emailAddress, Integer PersonId) {
        this.emailId = emailId;
        this.emailAddress = emailAddress;
        this.PersonId = PersonId;
    }

    public Email(String emailAddress, Integer PersonId) {
        this.emailAddress = emailAddress;
        this.PersonId = PersonId;
    }

    public void setEmailId(Integer emailId) {
        this.emailId = emailId;
    }

    public void setEmailAdress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setPersonId(Integer PersonId) {
        this.PersonId = PersonId;
    }

    public Integer getEmailId() {
        return emailId;
    }

    public String getEmailAdress() {
        return emailAddress;
    }

    public Integer getPersonId() {
        return PersonId;
    }   
}
