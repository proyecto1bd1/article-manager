/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

/**
 *
 * @author Daniel G
 */
public class Author {
    private Integer AuthorId;
    private Integer AuthorTypeId;

    public Author() {
    }
    
    

    public Author(Integer AuthorTypeId) {
        this.AuthorTypeId = AuthorTypeId;
    }
    
    public Author(Integer AuthorId, Integer AuthorTypeId) {
        this.AuthorId = AuthorId;
        this.AuthorTypeId = AuthorTypeId;
    }

    public void setAuthorId(Integer AuthorId) {
        this.AuthorId = AuthorId;
    }

    public void setAuthorTypeId(Integer AuthorTypeId) {
        this.AuthorTypeId = AuthorTypeId;
    }

    public Integer getAuthorId() {
        return AuthorId;
    }

    public Integer getAuthorTypeId() {
        return AuthorTypeId;
    }
}
