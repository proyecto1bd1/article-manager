/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

/**
 *
 * @author Daniel G
 */
public class PxReviewsXArticle {
    
    private Integer PersonId;
    private Integer ArticleId;
    private Integer nStars;

    public PxReviewsXArticle() {
    }
    
    

    public PxReviewsXArticle(Integer PersonId, Integer ArticleId, Integer nStars) {
        this.PersonId = PersonId;
        this.ArticleId = ArticleId;
        this.nStars = nStars;
    }

    public void setPersonId(Integer PersonId) {
        this.PersonId = PersonId;
    }

    public void setArticleId(Integer ArticleId) {
        this.ArticleId = ArticleId;
    }

    public void setnStars(Integer nStars) {
        this.nStars = nStars;
    }

    public Integer getPersonId() {
        return PersonId;
    }

    public Integer getArticleId() {
        return ArticleId;
    }

    public Integer getnStars() {
        return nStars;
    }
    
    


    
}
