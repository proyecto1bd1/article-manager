/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

/**
 *
 * @author Daniel G
 */
public class Purchase {
    private Integer PurchaseId;
    private Integer PersonId;

    public Purchase() {
    }
    
    

    public Purchase(Integer PurchaseId, Integer PersonId) {
        this.PurchaseId = PurchaseId;
        this.PersonId = PersonId;
    }

    public Purchase(Integer PersonId) {
        this.PersonId = PersonId;
    }

    public void setPurchaseId(Integer PurchaseId) {
        this.PurchaseId = PurchaseId;
    }

    public void setPersonId(Integer PersonId) {
        this.PersonId = PersonId;
    }

    public Integer getPurchaseId() {
        return PurchaseId;
    }

    public Integer getPersonId() {
        return PersonId;
    } 
}
