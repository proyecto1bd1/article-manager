create or replace PACKAGE authortypeManagement IS
PROCEDURE createAuthortype(p_Description IN VARCHAR2);
PROCEDURE updateAuthortype(p_AuthorTypeId IN NUMBER,p_Description IN VARCHAR2);
PROCEDURE deleteAuthortype(p_AuthorTypeId IN NUMBER);
PROCEDURE getAuthortype(p_AuthorTypeId IN NUMBER, pRecordSet OUT SYS_REFCURSOR);
END authortypeManagement;
/

create or replace PACKAGE BODY authortypeManagement AS
PROCEDURE createAuthortype(p_Description IN VARCHAR2) IS
BEGIN
INSERT INTO AUTHORTYPE (AuthorTypeId,Description)
VALUES (s_authortype.nextval, p_Description);
COMMIT;
END;


PROCEDURE updateAuthortype(p_AuthorTypeId IN NUMBER,p_Description IN VARCHAR2) IS
BEGIN
UPDATE AUTHORTYPE SET
Description = p_Description
WHERE AuthorTypeId = p_AuthorTypeId;
COMMIT;
END;


PROCEDURE deleteAuthortype(p_AuthorTypeId IN NUMBER) IS
BEGIN
DELETE FROM AUTHORTYPE
WHERE AuthorTypeId = p_AuthorTypeId;
COMMIT;
END;


PROCEDURE getAuthortype(p_AuthorTypeId IN NUMBER, pRecordSet OUT SYS_REFCURSOR) IS
BEGIN
OPEN pRecordSet FOR
SELECT AuthorTypeId,Description
FROM AUTHORTYPE
WHERE AuthorTypeId = NVL(p_AuthorTypeId, AuthorTypeId);
END;
END authortypeManagement;