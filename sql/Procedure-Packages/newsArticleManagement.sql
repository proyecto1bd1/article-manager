CREATE OR REPLACE PACKAGE newsarticleManagement IS
PROCEDURE createNewsarticle(p_Title IN VARCHAR2,p_Text IN VARCHAR2,p_PublicationDate IN DATE,p_Photo IN VARCHAR2,p_AuthorId IN NUMBER,p_ArticleTypeId IN NUMBER,p_NewsPaperId IN NUMBER,p_CommitteeIdApprov IN NUMBER);
PROCEDURE updateNewsarticle(p_ArticleId IN NUMBER,p_Title IN VARCHAR2,p_Text IN VARCHAR2,p_PublicationDate IN DATE,p_Photo IN VARCHAR2,p_AuthorId IN NUMBER,p_ArticleTypeId IN NUMBER,p_NewsPaperId IN NUMBER,p_CommitteeIdApprov IN NUMBER);
PROCEDURE deleteNewsarticle(p_ArticleId IN NUMBER);
PROCEDURE getNewsarticle(p_ArticleId IN NUMBER, pRecordSet OUT SYS_REFCURSOR);
END newsarticleManagement;
/

CREATE OR REPLACE PACKAGE BODY newsarticleManagement AS
PROCEDURE createNewsarticle(p_Title IN VARCHAR2,p_Text IN VARCHAR2,p_PublicationDate IN DATE,p_Photo IN VARCHAR2,p_AuthorId IN NUMBER,p_ArticleTypeId IN NUMBER,p_NewsPaperId IN NUMBER,p_CommitteeIdApprov IN NUMBER) IS
BEGIN
INSERT INTO NEWSARTICLE (ArticleId,Title,Text,PublicationDate,Photo,AuthorId,ArticleTypeId,NewsPaperId,CommitteeIdApprov)
VALUES (s_newsarticle.nextval, p_Title,p_Text,p_PublicationDate,p_Photo,p_AuthorId,p_ArticleTypeId,p_NewsPaperId,p_CommitteeIdApprov);
COMMIT;
END;


PROCEDURE updateNewsarticle(p_ArticleId IN NUMBER,p_Title IN VARCHAR2,p_Text IN VARCHAR2,p_PublicationDate IN DATE,p_Photo IN VARCHAR2,p_AuthorId IN NUMBER,p_ArticleTypeId IN NUMBER,p_NewsPaperId IN NUMBER,p_CommitteeIdApprov IN NUMBER) IS
BEGIN
UPDATE NEWSARTICLE SET
Title = p_Title,
Text = p_Text,
PublicationDate = p_PublicationDate,
Photo = p_Photo,
AuthorId = p_AuthorId,
ArticleTypeId = p_ArticleTypeId,
NewsPaperId = p_NewsPaperId,
CommitteeIdApprov = p_CommitteeIdApprov
WHERE ArticleId = p_ArticleId;
COMMIT;
END;


PROCEDURE deleteNewsarticle(p_ArticleId IN NUMBER) IS
BEGIN
DELETE FROM NEWSARTICLE
WHERE ArticleId = p_ArticleId;
COMMIT;
END;


PROCEDURE getNewsarticle(p_ArticleId IN NUMBER, pRecordSet OUT SYS_REFCURSOR) IS
BEGIN
OPEN pRecordSet FOR
SELECT ArticleId,Title,Text,PublicationDate,Photo,AuthorId,ArticleTypeId,NewsPaperId,CommitteeIdApprov
FROM NEWSARTICLE
WHERE ArticleId = NVL(p_ArticleId, ArticleId);
END;
END newsarticleManagement;
/