CREATE SEQUENCE S_ADMIN
            START WITH 0
            INCREMENT BY 1
            MINVALUE 0
            MAXVALUE 10000000
            NOCACHE
            NOCYCLE;

CREATE OR REPLACE PACKAGE adminManagement IS
PROCEDURE createAdmin(p_adminId IN NUMBER);
PROCEDURE deleteAdmin(p_adminId IN NUMBER);
PROCEDURE getAdmin(p_adminId IN NUMBER, pRecordSet OUT SYS_REFCURSOR);
END adminManagement;
/

CREATE OR REPLACE PACKAGE BODY adminManagement AS
PROCEDURE createAdmin(p_adminId IN NUMBER) IS
BEGIN
INSERT INTO ADMIN (adminId)
VALUES (s_admin.nextval);
COMMIT;
END;


PROCEDURE deleteAdmin(p_adminId IN NUMBER) IS
BEGIN
DELETE FROM ADMIN
WHERE adminId= p_adminId;
COMMIT;
END;

PROCEDURE getAdmin(p_adminId IN NUMBER, pRecordSet OUT SYS_REFCURSOR) IS
BEGIN
OPEN pRecordSet FOR
SELECT adminId
FROM ADMIN
WHERE adminId = NVL(p_adminId, adminId);
END;
END adminManagement;
/