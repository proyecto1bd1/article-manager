CREATE OR REPLACE PACKAGE telephoneManagement IS
PROCEDURE createTelephone(p_phoneNumber IN VARCHAR2,p_personId IN NUMBER);
PROCEDURE updateTelephone(p_telephoneid IN NUMBER,p_phoneNumber IN VARCHAR2,p_personId IN NUMBER);
PROCEDURE deleteTelephone(p_telephoneid IN NUMBER);
PROCEDURE getTelephone(p_telephoneid IN NUMBER, pRecordSet OUT SYS_REFCURSOR);
END telephoneManagement;

CREATE OR REPLACE PACKAGE BODY telephoneManagement AS
PROCEDURE createTelephone(p_phoneNumber IN VARCHAR2,p_personId IN NUMBER) IS
BEGIN
INSERT INTO TELEPHONE (telephoneid,phoneNumber,personId)
VALUES (s_telephone.nextval, p_phoneNumber,p_personId);
COMMIT;
END;

PROCEDURE updateTelephone(p_telephoneid IN NUMBER,p_phoneNumber IN VARCHAR2,p_personId IN NUMBER) IS
BEGIN
UPDATE TELEPHONE SET
phoneNumber = p_phoneNumber,
personId = p_personId
WHERE telephoneid = p_telephoneid;
COMMIT;
END;

PROCEDURE deleteTelephone(p_telephoneid IN NUMBER) IS
BEGIN
DELETE FROM TELEPHONE
WHERE telephoneid = p_telephoneid;
COMMIT;
END;

PROCEDURE getTelephone(p_telephoneid IN NUMBER, pRecordSet OUT SYS_REFCURSOR) IS
BEGIN
OPEN pRecordSet FOR
SELECT telephoneid,phoneNumber,personId
FROM TELEPHONE
WHERE telephoneid = NVL(p_telephoneid, telephoneid);
END;
END telephoneManagement;