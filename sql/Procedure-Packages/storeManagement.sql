CREATE OR REPLACE PACKAGE storeManagement IS
PROCEDURE createStore(p_Description IN VARCHAR2,p_NewspaperId IN NUMBER);
PROCEDURE updateStore(p_StoreId IN NUMBER,p_Description IN VARCHAR2,p_NewspaperId IN NUMBER);
PROCEDURE deleteStore(p_StoreId IN NUMBER);
PROCEDURE getStore(p_StoreId IN NUMBER, pRecordSet OUT SYS_REFCURSOR);
END storeManagement;
/

CREATE OR REPLACE PACKAGE BODY storeManagement AS
PROCEDURE createStore(p_Description IN VARCHAR2,p_NewspaperId IN NUMBER) IS
BEGIN
INSERT INTO STORE (StoreId,Description,NewspaperId)
VALUES (s_store.nextval, p_Description,p_NewspaperId);
COMMIT;
END;


PROCEDURE updateStore(p_StoreId IN NUMBER,p_Description IN VARCHAR2,p_NewspaperId IN NUMBER) IS
BEGIN
UPDATE STORE SET
Description = p_Description,
NewspaperId = p_NewspaperId
WHERE StoreId = p_StoreId;
COMMIT;
END;


PROCEDURE deleteStore(p_StoreId IN NUMBER) IS
BEGIN
DELETE FROM STORE
WHERE StoreId = p_StoreId;
COMMIT;
END;

PROCEDURE getStore(p_StoreId IN NUMBER, pRecordSet OUT SYS_REFCURSOR) IS
BEGIN
OPEN pRecordSet FOR
SELECT StoreId,Description,NewspaperId
FROM STORE
WHERE StoreId = NVL(p_StoreId, StoreId);
END;
END storeManagement;
/