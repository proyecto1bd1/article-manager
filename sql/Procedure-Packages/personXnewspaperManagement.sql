CREATE OR REPLACE PACKAGE personxnewspaperManagement IS
PROCEDURE createPersonxnewspaper(p_NewsPaperId IN NUMBER, p_PersonId IN NUMBER);
PROCEDURE deletePersonxnewspaper(p_NewsPaperId IN NUMBER, p_PersonId IN NUMBER);
PROCEDURE getPersonxnewspaper(p_NewsPaperId IN NUMBER, p_PersonId IN NUMBER, pRecordSet OUT SYS_REFCURSOR);
END personxnewspaperManagement;
/

CREATE OR REPLACE PACKAGE BODY personxnewspaperManagement AS
PROCEDURE createPersonxnewspaper(p_NewsPaperId IN NUMBER, p_PersonId IN NUMBER) IS
BEGIN
INSERT INTO PERSONXNEWSPAPER (NewsPaperId,PersonId)
VALUES (p_NewsPaperId, p_PersonId);
COMMIT;
END;

PROCEDURE deletePersonxnewspaper(p_NewsPaperId IN NUMBER, p_PersonId IN NUMBER) IS
BEGIN
DELETE FROM PERSONXNEWSPAPER
WHERE NewsPaperId = p_NewsPaperId AND PersonId = p_PersonId;
COMMIT;
END;

PROCEDURE getPersonxnewspaper(p_NewsPaperId IN NUMBER,p_PersonId IN NUMBER, pRecordSet OUT SYS_REFCURSOR) IS
BEGIN
OPEN pRecordSet FOR
SELECT NewsPaperId,PersonId
FROM PERSONXNEWSPAPER
WHERE NewsPaperId = NVL(p_NewsPaperId, NewsPaperId) AND PersonId = NVL(p_PersonId, PersonId);
END;
END personxnewspaperManagement;
/
