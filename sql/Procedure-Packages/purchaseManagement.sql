CREATE OR REPLACE PACKAGE purchaseManagement IS
PROCEDURE createPurchase(p_PersonId IN NUMBER);
PROCEDURE updatePurchase(p_PurchaseId IN NUMBER,p_PersonId IN NUMBER);
PROCEDURE deletePurchase(p_PurchaseId IN NUMBER);
PROCEDURE getPurchase(p_PurchaseId IN NUMBER, pRecordSet OUT SYS_REFCURSOR);
END purchaseManagement;
/

CREATE OR REPLACE PACKAGE BODY purchaseManagement AS
PROCEDURE createPurchase(p_PersonId IN NUMBER) IS
BEGIN
INSERT INTO PURCHASE (PurchaseId,PersonId)
VALUES (s_purchase.nextval, p_PersonId);
COMMIT;
END;


PROCEDURE updatePurchase(p_PurchaseId IN NUMBER,p_PersonId IN NUMBER) IS
BEGIN
UPDATE PURCHASE SET
PersonId = p_PersonId
WHERE PurchaseId = p_PurchaseId;
COMMIT;
END;


PROCEDURE deletePurchase(p_PurchaseId IN NUMBER) IS
BEGIN
DELETE FROM PURCHASE
WHERE PurchaseId = p_PurchaseId;
COMMIT;
END;


PROCEDURE getPurchase(p_PurchaseId IN NUMBER, pRecordSet OUT SYS_REFCURSOR) IS
BEGIN
OPEN pRecordSet FOR
SELECT PurchaseId,PersonId
FROM PURCHASE
WHERE PurchaseId = NVL(p_PurchaseId, PurchaseId);
END;
END purchaseManagement;
/