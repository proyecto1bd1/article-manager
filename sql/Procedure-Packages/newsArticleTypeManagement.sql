
CREATE OR REPLACE PACKAGE newsarticletypeManagement IS
PROCEDURE createNewsarticletype(p_Description IN VARCHAR2);
PROCEDURE updateNewsarticletype(p_TypeId IN NUMBER,p_Description IN VARCHAR2);
PROCEDURE deleteNewsarticletype(p_TypeId IN NUMBER);
PROCEDURE getNewsarticletype(p_TypeId IN NUMBER, pRecordSet OUT SYS_REFCURSOR);
END newsarticletypeManagement;
/

CREATE OR REPLACE PACKAGE BODY newsarticletypeManagement AS
PROCEDURE createNewsarticletype(p_Description IN VARCHAR2) IS
BEGIN
INSERT INTO NEWSARTICLETYPE (TypeId,Description)
VALUES (s_newsarticletype.nextval, p_Description);
COMMIT;
END;


PROCEDURE updateNewsarticletype(p_TypeId IN NUMBER,p_Description IN VARCHAR2) IS
BEGIN
UPDATE NEWSARTICLETYPE SET
Description = p_Description
WHERE TypeId = p_TypeId;
COMMIT;
END;


PROCEDURE deleteNewsarticletype(p_TypeId IN NUMBER) IS
BEGIN
DELETE FROM NEWSARTICLETYPE
WHERE TypeId = p_TypeId;
COMMIT;
END;


PROCEDURE getNewsarticletype(p_TypeId IN NUMBER, pRecordSet OUT SYS_REFCURSOR) IS
BEGIN
OPEN pRecordSet FOR
SELECT TypeId,Description
FROM NEWSARTICLETYPE
WHERE TypeId = NVL(p_TypeId, TypeId);
END;
END newsarticletypeManagement;
/
