CREATE OR REPLACE PACKAGE parameterManagement IS
PROCEDURE createParameter(p_Name IN VARCHAR2,p_Description IN VARCHAR2,p_Value IN NUMBER,p_UniversityId IN NUMBER);
PROCEDURE updateParameter(p_ParameterId IN NUMBER,p_Name IN VARCHAR2,p_Description IN VARCHAR2,p_Value IN NUMBER,p_UniversityId IN NUMBER);
PROCEDURE deleteParameter(p_ParameterId IN NUMBER);
PROCEDURE getParameter(p_ParameterId IN NUMBER, pRecordSet OUT SYS_REFCURSOR);
END parameterManagement;
/

CREATE OR REPLACE PACKAGE BODY parameterManagement AS
PROCEDURE createParameter(p_Name IN VARCHAR2,p_Description IN VARCHAR2,p_Value IN NUMBER,p_UniversityId IN NUMBER) IS
BEGIN
INSERT INTO PARAMETER (ParameterId,Name,Description,Value,UniversityId)
VALUES (s_parameter.nextval, p_Name,p_Description,p_Value,p_UniversityId);
COMMIT;
END;


PROCEDURE updateParameter(p_ParameterId IN NUMBER,p_Name IN VARCHAR2,p_Description IN VARCHAR2,p_Value IN NUMBER,p_UniversityId IN NUMBER) IS
BEGIN
UPDATE PARAMETER SET
Name = p_Name,
Description = p_Description,
Value = p_Value,
UniversityId = p_UniversityId
WHERE ParameterId = p_ParameterId;
COMMIT;
END;


PROCEDURE deleteParameter(p_ParameterId IN NUMBER) IS
BEGIN
DELETE FROM PARAMETER
WHERE ParameterId = p_ParameterId;
COMMIT;
END;


PROCEDURE getParameter(p_ParameterId IN NUMBER, pRecordSet OUT SYS_REFCURSOR) IS
BEGIN
OPEN pRecordSet FOR
SELECT ParameterId,Name,Description,Value,UniversityId
FROM PARAMETER
WHERE ParameterId = NVL(p_ParameterId, ParameterId);
END;
END parameterManagement;
/

