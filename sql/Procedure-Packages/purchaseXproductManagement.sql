CREATE OR REPLACE PACKAGE purchasexproductManagement IS
PROCEDURE createPurchasexproduct( p_PurchaseId IN NUMBER, p_ProductId IN NUMBER);
PROCEDURE deletePurchasexproduct(p_PurchaseId IN NUMBER, p_ProductId IN NUMBER);
PROCEDURE getPurchasexproduct(p_PurchaseId IN NUMBER, p_ProductId IN NUMBER, pRecordSet OUT SYS_REFCURSOR);
END purchasexproductManagement;
/

CREATE OR REPLACE PACKAGE BODY purchasexproductManagement AS
PROCEDURE createPurchasexproduct(p_PurchaseId IN NUMBER, p_ProductId IN NUMBER) IS
BEGIN
INSERT INTO PURCHASEXPRODUCT (PurchaseId,ProductId)
VALUES (p_PurchaseId, p_ProductId);
COMMIT;
END;

PROCEDURE deletePurchasexproduct(p_PurchaseId IN NUMBER, p_ProductId IN NUMBER) IS
BEGIN
DELETE FROM PURCHASEXPRODUCT
WHERE PurchaseId = p_PurchaseId AND ProductId = p_ProductId ;
COMMIT;
END;

PROCEDURE getPurchasexproduct(p_PurchaseId IN NUMBER, p_ProductId IN NUMBER, pRecordSet OUT SYS_REFCURSOR) IS
BEGIN
OPEN pRecordSet FOR
SELECT PurchaseId,ProductId
FROM PURCHASEXPRODUCT
WHERE PurchaseId = NVL(p_PurchaseId, PurchaseId) AND ProductId = NVL (p_ProductId, ProductId) ;
END;
END purchasexproductManagement;
/
