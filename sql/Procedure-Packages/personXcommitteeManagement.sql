CREATE OR REPLACE PACKAGE personxcommitteeManagement IS
PROCEDURE createPersonxcommittee(p_PersonId IN NUMBER, p_CommitteeId IN NUMBER);
PROCEDURE deletePersonxcommittee(p_PersonId IN NUMBER, p_CommitteeId IN NUMBER);
PROCEDURE getPersonxcommittee(p_PersonId IN NUMBER, p_CommitteeId IN NUMBER, pRecordSet OUT SYS_REFCURSOR);
END personxcommitteeManagement;
/

CREATE OR REPLACE PACKAGE BODY personxcommitteeManagement AS
PROCEDURE createPersonxcommittee(p_PersonId IN NUMBER, p_CommitteeId IN NUMBER) IS
BEGIN
INSERT INTO PERSONXCOMMITTEE (PersonId,CommitteeId)
VALUES (p_PersonId, p_CommitteeId);
COMMIT;
END;


PROCEDURE deletePersonxcommittee(p_PersonId IN NUMBER, p_CommitteeId IN NUMBER) IS
BEGIN
DELETE FROM PERSONXCOMMITTEE
WHERE PersonId = p_PersonId AND CommitteeId = p_CommitteeId;
COMMIT;
END;


PROCEDURE getPersonxcommittee(p_PersonId IN NUMBER, p_CommitteeId IN NUMBER, pRecordSet OUT SYS_REFCURSOR) IS
BEGIN
OPEN pRecordSet FOR
SELECT PersonId,CommitteeId
FROM PERSONXCOMMITTEE
WHERE PersonId = NVL(p_PersonId, PersonId) AND CommitteeId = NVL(p_CommitteeId,CommitteeId);
END;
END personxcommitteeManagement;
/
