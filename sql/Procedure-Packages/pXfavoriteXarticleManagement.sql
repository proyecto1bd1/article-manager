CREATE OR REPLACE PACKAGE pxfavoritexarticleManagement IS
PROCEDURE createPxfavoritexarticle(p_PersonId IN NUMBER, p_ArticleId IN NUMBER);
PROCEDURE deletePxfavoritexarticle(p_PersonId IN NUMBER, p_ArticleId IN NUMBER);
PROCEDURE getPxfavoritexarticle(p_PersonId IN NUMBER, p_ArticleId IN NUMBER, pRecordSet OUT SYS_REFCURSOR);
END pxfavoritexarticleManagement;
/

CREATE OR REPLACE PACKAGE BODY pxfavoritexarticleManagement AS
PROCEDURE createPxfavoritexarticle(p_PersonId IN NUMBER, p_ArticleId IN NUMBER) IS
BEGIN
INSERT INTO PXFAVORITEXARTICLE (PersonId,ArticleId,deleted)
VALUES (p_PersonId, p_ArticleId,0);
COMMIT;
END;

PROCEDURE deletePxfavoritexarticle(p_PersonId IN NUMBER, p_ArticleId IN NUMBER) IS
BEGIN
UPDATE PXFAVORITEXARTICLE
SET deleted = 1
WHERE PersonId = p_PersonId AND ArticleId = p_ArticleId;
COMMIT;
END;


PROCEDURE getPxfavoritexarticle(p_PersonId IN NUMBER, p_ArticleId IN NUMBER, pRecordSet OUT SYS_REFCURSOR) IS
BEGIN
OPEN pRecordSet FOR
SELECT PersonId,ArticleId,deleted
FROM PXFAVORITEXARTICLE
WHERE PersonId = NVL(p_PersonId, PersonId) AND ArticleId = NVL(p_ArticleId, ArticleId);
END;
END pxfavoritexarticleManagement;
/
