CREATE OR REPLACE PACKAGE pxreviewsxarticleManagement IS
PROCEDURE createPxreviewsxarticle(p_PersonId IN NUMBER,p_ArticleId IN NUMBER,p_nStars IN NUMBER);
PROCEDURE updatePxreviewsxarticle(p_PersonId IN NUMBER,p_ArticleId IN NUMBER,p_nStars IN NUMBER);
PROCEDURE deletePxreviewsxarticle(p_PersonId IN NUMBER,p_ArticleId IN NUMBER);
PROCEDURE getPxreviewsxarticle(p_PersonId IN NUMBER,p_ArticleId IN NUMBER, pRecordSet OUT SYS_REFCURSOR);
END pxreviewsxarticleManagement;
/

CREATE OR REPLACE PACKAGE BODY pxreviewsxarticleManagement AS
PROCEDURE createPxreviewsxarticle(p_PersonId IN NUMBER,p_ArticleId IN NUMBER,p_nStars IN NUMBER) IS
BEGIN
INSERT INTO PXREVIEWSXARTICLE (PersonId,ArticleId,nStars)
VALUES (p_PersonId, p_ArticleId,p_nStars);
COMMIT;
END;


PROCEDURE updatePxreviewsxarticle(p_PersonId IN NUMBER,p_ArticleId IN NUMBER,p_nStars IN NUMBER) IS
BEGIN
UPDATE PXREVIEWSXARTICLE SET
nStars = p_nStars
WHERE PersonId = p_PersonId AND ArticleId = p_ArticleId;
COMMIT;
END;

PROCEDURE deletePxreviewsxarticle(p_PersonId IN NUMBER,p_ArticleId IN NUMBER) IS
BEGIN
DELETE FROM PXREVIEWSXARTICLE
WHERE PersonId = p_PersonId AND ArticleId = p_ArticleId;
COMMIT;
END;


PROCEDURE getPxreviewsxarticle(p_PersonId IN NUMBER,p_ArticleId IN NUMBER, pRecordSet OUT SYS_REFCURSOR) IS
BEGIN
OPEN pRecordSet FOR
SELECT PersonId,ArticleId,nStars
FROM PXREVIEWSXARTICLE
WHERE PersonId = NVL(p_PersonId, PersonId) AND ArticleId = NVL(p_ArticleId, ArticleId);
END;
END pxreviewsxarticleManagement;
/
