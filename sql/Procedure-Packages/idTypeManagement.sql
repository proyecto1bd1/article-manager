CREATE OR REPLACE PACKAGE idtypeManagement IS
PROCEDURE createIdtype(p_Description IN VARCHAR2,p_Mask IN VARCHAR2);
PROCEDURE updateIdtype(p_TypeId IN NUMBER, p_Description IN VARCHAR2, p_Mask IN VARCHAR2);
PROCEDURE deleteIdtype(p_TypeId IN NUMBER);
PROCEDURE getIdtype(p_TypeId IN NUMBER, pRecordSet OUT SYS_REFCURSOR);
END idtypeManagement;
/

CREATE OR REPLACE PACKAGE BODY idtypeManagement AS
PROCEDURE createIdtype(p_Description IN VARCHAR2,p_Mask IN VARCHAR2) IS
BEGIN
INSERT INTO IDTYPE (TypeId,Description,Mask)
VALUES (s_idtype.nextval, p_Description,p_Mask);
COMMIT;
END;


PROCEDURE updateIdtype(p_TypeId IN NUMBER,p_Description IN VARCHAR2,p_Mask IN VARCHAR2) IS
BEGIN
UPDATE IDTYPE SET
Description = p_Description,
Mask = p_Mask
WHERE TypeId = p_TypeId;
COMMIT;
END;


PROCEDURE deleteIdtype(p_TypeId IN NUMBER) IS
BEGIN
DELETE FROM IDTYPE
WHERE TypeId = p_TypeId;
COMMIT;
END;


PROCEDURE getIdtype(p_TypeId IN NUMBER, pRecordSet OUT SYS_REFCURSOR) IS
BEGIN
OPEN pRecordSet FOR
SELECT TypeId,Description,Mask
FROM IDTYPE
WHERE TypeId = NVL(p_TypeId, TypeId);
END;
END idtypeManagement;
/
