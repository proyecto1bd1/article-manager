CREATE OR REPLACE PACKAGE logManagement IS
PROCEDURE createLog(p_FieldName IN VARCHAR2,p_TableName IN VARCHAR2,p_CurrentValue IN VARCHAR2,p_PreviousValue IN VARCHAR2,p_ArticleId IN NUMBER);
PROCEDURE updateLog(p_LogId IN NUMBER,p_FieldName IN VARCHAR2,p_TableName IN VARCHAR2,p_CurrentValue IN VARCHAR2,p_PreviousValue IN VARCHAR2,p_ArticleId IN NUMBER);
PROCEDURE deleteLog(p_LogId IN NUMBER);
PROCEDURE getLog(p_LogId IN NUMBER, pRecordSet OUT SYS_REFCURSOR);
END logManagement;
/

CREATE OR REPLACE PACKAGE BODY logManagement AS
PROCEDURE createLog(p_FieldName IN VARCHAR2,p_TableName IN VARCHAR2,p_CurrentValue IN VARCHAR2,p_PreviousValue IN VARCHAR2,p_ArticleId IN NUMBER) IS
BEGIN
INSERT INTO LOG (LogId,FieldName,TableName,CurrentValue,PreviousValue,ArticleId)
VALUES (s_log.nextval, p_FieldName,p_TableName,p_CurrentValue,p_PreviousValue,p_ArticleId);
COMMIT;
END;


PROCEDURE updateLog(p_LogId IN NUMBER,p_FieldName IN VARCHAR2,p_TableName IN VARCHAR2,p_CurrentValue IN VARCHAR2,p_PreviousValue IN VARCHAR2,p_ArticleId IN NUMBER) IS
BEGIN
UPDATE LOG SET
FieldName = p_FieldName,
TableName = p_TableName,
CurrentValue = p_CurrentValue,
PreviousValue = p_PreviousValue,
ArticleId = p_ArticleId
WHERE LogId = p_LogId;
COMMIT;
END;


PROCEDURE deleteLog(p_LogId IN NUMBER) IS
BEGIN
DELETE FROM LOG
WHERE LogId = p_LogId;
COMMIT;
END;


PROCEDURE getLog(p_LogId IN NUMBER, pRecordSet OUT SYS_REFCURSOR) IS
BEGIN
OPEN pRecordSet FOR
SELECT LogId,FieldName,TableName,CurrentValue,PreviousValue,ArticleId
FROM LOG
WHERE LogId = NVL(p_LogId, LogId);
END;
END logManagement;
/