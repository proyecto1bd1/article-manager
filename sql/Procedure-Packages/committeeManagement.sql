CREATE OR REPLACE PACKAGE committeeManagement IS
PROCEDURE createCommittee(p_Description IN VARCHAR2);
PROCEDURE updateCommittee(p_CommitteeId IN NUMBER,p_Description IN VARCHAR2);
PROCEDURE deleteCommittee(p_CommitteeId IN NUMBER);
PROCEDURE getCommittee(p_CommitteeId IN NUMBER, pRecordSet OUT SYS_REFCURSOR);
END committeeManagement;
/

CREATE OR REPLACE PACKAGE BODY committeeManagement AS
PROCEDURE createCommittee(p_Description IN VARCHAR2) IS
BEGIN
INSERT INTO COMMITTEE (CommitteeId,Description)
VALUES (s_committee.nextval, p_Description);
COMMIT;
END;


PROCEDURE updateCommittee(p_CommitteeId IN NUMBER,p_Description IN VARCHAR2) IS
BEGIN
UPDATE COMMITTEE SET
Description = p_Description
WHERE CommitteeId = p_CommitteeId;
COMMIT;
END;


PROCEDURE deleteCommittee(p_CommitteeId IN NUMBER) IS
BEGIN
DELETE FROM COMMITTEE
WHERE CommitteeId = p_CommitteeId;
COMMIT;
END;


PROCEDURE getCommittee(p_CommitteeId IN NUMBER, pRecordSet OUT SYS_REFCURSOR) IS
BEGIN
OPEN pRecordSet FOR
SELECT CommitteeId,Description
FROM COMMITTEE
WHERE CommitteeId = NVL(p_CommitteeId, CommitteeId);
END;
END committeeManagement;
/



