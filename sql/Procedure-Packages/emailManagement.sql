CREATE OR REPLACE PACKAGE emailManagement IS
PROCEDURE createEmail(p_emailAddress IN VARCHAR2,p_PersonId IN NUMBER);
PROCEDURE updateEmail(p_emailId IN NUMBER, p_emailAddress IN VARCHAR2, p_PersonId IN NUMBER);
PROCEDURE deleteEmail(p_emailId IN NUMBER);
PROCEDURE getEmail(p_emailId IN NUMBER, pRecordSet OUT SYS_REFCURSOR);
END emailManagement;
/

CREATE OR REPLACE PACKAGE BODY emailManagement AS
PROCEDURE createEmail(p_emailAddress IN VARCHAR2, p_PersonId IN NUMBER) IS
BEGIN
INSERT INTO EMAIL (emailId,emailAddress,PersonId)
VALUES (s_email.nextval, p_emailAddress,p_PersonId);
COMMIT;
END;


PROCEDURE updateEmail(p_emailId IN NUMBER, p_emailAddress IN VARCHAR2, p_PersonId IN NUMBER) IS
BEGIN
UPDATE EMAIL SET
emailAddress = p_emailAddress,
PersonId = p_PersonId
WHERE emailId = p_emailId;
COMMIT;
END;


PROCEDURE deleteEmail(p_emailId IN NUMBER) IS
BEGIN
DELETE FROM EMAIL
WHERE emailId = p_emailId;
COMMIT;
END;


PROCEDURE getEmail(p_emailId IN NUMBER, pRecordSet OUT SYS_REFCURSOR) IS
BEGIN
OPEN pRecordSet FOR
SELECT emailId,emailAddress,PersonId
FROM EMAIL
WHERE emailId = NVL(p_emailId, emailId);
END;
END emailManagement;
/