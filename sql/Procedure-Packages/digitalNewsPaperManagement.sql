CREATE OR REPLACE PACKAGE digitalnewspaperManagement IS
PROCEDURE createDigitalnewspaper(p_NewsPaperName IN VARCHAR2,p_CommitteeId IN NUMBER);
PROCEDURE updateDigitalnewspaper(p_NewsPaperId IN NUMBER,p_NewsPaperName IN VARCHAR2,p_CommitteeId IN NUMBER);
PROCEDURE deleteDigitalnewspaper(p_NewsPaperId IN NUMBER);
PROCEDURE getDigitalnewspaper(p_NewsPaperId IN NUMBER, pRecordSet OUT SYS_REFCURSOR);
END digitalnewspaperManagement;
/

CREATE OR REPLACE PACKAGE BODY digitalnewspaperManagement AS
PROCEDURE createDigitalnewspaper(p_NewsPaperName IN VARCHAR2,p_CommitteeId IN NUMBER) IS
BEGIN
INSERT INTO DIGITALNEWSPAPER (NewsPaperId,NewsPaperName,CommitteeId)
VALUES (s_digitalnewspaper.nextval, p_NewsPaperName,p_CommitteeId);
COMMIT;
END;


PROCEDURE updateDigitalnewspaper(p_NewsPaperId IN NUMBER,p_NewsPaperName IN VARCHAR2,p_CommitteeId IN NUMBER) IS
BEGIN
UPDATE DIGITALNEWSPAPER SET
NewsPaperName = p_NewsPaperName,
CommitteeId = p_CommitteeId
WHERE NewsPaperId = p_NewsPaperId;
COMMIT;
END;


PROCEDURE deleteDigitalnewspaper(p_NewsPaperId IN NUMBER) IS
BEGIN
DELETE FROM DIGITALNEWSPAPER
WHERE NewsPaperId = p_NewsPaperId;
COMMIT;
END;


PROCEDURE getDigitalnewspaper(p_NewsPaperId IN NUMBER, pRecordSet OUT SYS_REFCURSOR) IS
BEGIN
OPEN pRecordSet FOR
SELECT NewsPaperId,NewsPaperName,CommitteeId
FROM DIGITALNEWSPAPER
WHERE NewsPaperId = NVL(p_NewsPaperId, NewsPaperId);
END;
END digitalnewspaperManagement;
/




