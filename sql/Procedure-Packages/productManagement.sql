CREATE OR REPLACE PACKAGE productManagement IS
PROCEDURE createProduct(p_Name IN VARCHAR2,p_NumberInStock IN NUMBER,p_StoreId IN NUMBER);
PROCEDURE updateProduct(p_ProductId IN NUMBER,p_Name IN VARCHAR2,p_NumberInStock IN NUMBER,p_StoreId IN NUMBER);
PROCEDURE deleteProduct(p_ProductId IN NUMBER);
PROCEDURE getProduct(p_ProductId IN NUMBER, pRecordSet OUT SYS_REFCURSOR);
END productManagement;
/

CREATE OR REPLACE PACKAGE BODY productManagement AS
PROCEDURE createProduct(p_Name IN VARCHAR2,p_NumberInStock IN NUMBER,p_StoreId IN NUMBER) IS
BEGIN
INSERT INTO PRODUCT (ProductId,Name,NumberInStock,StoreId)
VALUES (s_product.nextval, p_Name,p_NumberInStock,p_StoreId);
COMMIT;
END;


PROCEDURE updateProduct(p_ProductId IN NUMBER,p_Name IN VARCHAR2,p_NumberInStock IN NUMBER,p_StoreId IN NUMBER) IS
BEGIN
UPDATE PRODUCT SET
Name = p_Name,
NumberInStock = p_NumberInStock,
StoreId = p_StoreId
WHERE ProductId = p_ProductId;
COMMIT;
END;


PROCEDURE deleteProduct(p_ProductId IN NUMBER) IS
BEGIN
DELETE FROM PRODUCT
WHERE ProductId = p_ProductId;
COMMIT;
END;


PROCEDURE getProduct(p_ProductId IN NUMBER, pRecordSet OUT SYS_REFCURSOR) IS
BEGIN
OPEN pRecordSet FOR
SELECT ProductId,Name,NumberInStock,StoreId
FROM PRODUCT
WHERE ProductId = NVL(p_ProductId, ProductId);
END;
END productManagement;
/
