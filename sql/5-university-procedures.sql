CREATE OR REPLACE PACKAGE universityManagement IS
PROCEDURE createUniversity(
    p_UniversityName IN VARCHAR2,
    p_LocalAddress IN VARCHAR2,
    p_DistrictId IN NUMBER);
PROCEDURE updateUniversity(
    p_UniversityId IN NUMBER,
    p_UniversityName IN VARCHAR2,
    p_LocalAddress IN VARCHAR2,
    p_DistrictId IN NUMBER);
PROCEDURE deleteUniversity(p_UniversityId IN NUMBER);
PROCEDURE getUniversities(p_UniversityId IN NUMBER, pRecordSet OUT SYS_REFCURSOR);
END;

CREATE OR REPLACE PACKAGE BODY universityManagement AS
PROCEDURE createUniversity(
    p_UniversityName IN VARCHAR2,
    p_LocalAddress IN VARCHAR2,
    p_DistrictId IN NUMBER) IS
BEGIN
    INSERT INTO University(UniversityId, UniversityName, LocalAddress, DistrictId)
    VALUES (s_university.nextval, p_UniversityName, p_LocalAddress, p_DistrictId);
    COMMIT;
END;
PROCEDURE updateUniversity(
    p_UniversityId IN NUMBER,
    p_UniversityName IN VARCHAR2,
    p_LocalAddress IN VARCHAR2,
    p_DistrictId IN NUMBER) IS
BEGIN
    UPDATE University
    SET UniversityName = p_UniversityName,
        LocalAddress = p_LocalAddress,
        DistrictId = p_DistrictId
    WHERE UniversityId = p_UniversityId;
    COMMIT;
END;

PROCEDURE deleteUniversity(p_UniversityId IN NUMBER) IS
BEGIN
    DELETE FROM University WHERE UniversityId = p_UniversityId;
    COMMIT;
END;

PROCEDURE getUniversities(p_UniversityId IN NUMBER, pRecordSet OUT SYS_REFCURSOR) IS
BEGIN
    OPEN pRecordSet FOR
        SELECT UniversityId,
               UniversityName,
               LocalAddress,
               DistrictId
        FROM University
        WHERE UniversityId = NVL(p_UniversityId, UniversityId);
END;
END;
