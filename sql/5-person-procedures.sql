CREATE OR REPLACE PACKAGE personManagement IS
PROCEDURE createPerson(
    p_IdCard IN NUMBER,
    p_Name IN VARCHAR2,
    p_FirstLastName IN VARCHAR2,
    p_SecondLastName IN VARCHAR2,
    p_Birthdate IN DATE,
    p_Photo IN VARCHAR2,
    p_LocalAddress IN VARCHAR2,
    p_Username IN VARCHAR2,
    p_Password IN VARCHAR2,
    p_GenderId IN NUMBER,
    p_TypeId IN NUMBER,
    p_DistrictId IN NUMBER);
PROCEDURE updatePerson(
    p_PersonId IN NUMBER,
    p_IdCard IN NUMBER,
    p_Name IN VARCHAR2,
    p_FirstLastName IN VARCHAR2,
    p_SecondLastName IN VARCHAR2,
    p_Birthdate IN DATE,
    p_Photo IN VARCHAR2,
    p_LocalAddress IN VARCHAR2,
    p_GenderId IN NUMBER,
    p_TypeId IN NUMBER,
    p_DistrictId IN NUMBER);
PROCEDURE deletePerson(p_PersonId IN NUMBER);
PROCEDURE getPersons(p_PersonId IN NUMBER, pRecordSet OUT SYS_REFCURSOR);
PROCEDURE loginUser(p_Username IN VARCHAR2, p_Password IN VARCHAR2, pRecordSet OUT SYS_REFCURSOR);
END;

CREATE OR REPLACE PACKAGE BODY personManagement AS
PROCEDURE createPerson(
    p_IdCard IN NUMBER,
    p_Name IN VARCHAR2,
    p_FirstLastName IN VARCHAR2,
    p_SecondLastName IN VARCHAR2,
    p_Birthdate IN DATE,
    p_Photo IN VARCHAR2,
    p_LocalAddress IN VARCHAR2,
    p_Username IN VARCHAR2,
    p_Password IN VARCHAR2,
    p_GenderId IN NUMBER,
    p_TypeId IN NUMBER,
    p_DistrictId IN NUMBER) IS
BEGIN
    INSERT INTO Person(
        PersonId,
        IdCard,
        "Name",
        FirstLastName,
        SecondLastName,
        Birthdate,
        Photo,
        LocalAddress,
        Username,
        "Password",
        GenderId,
        TypeId,
        DistrictId)
    VALUES(
        s_person.nextval,
        p_IdCard,
        p_Name,
        p_FirstLastName,
        p_SecondLastName,
        p_Birthdate,
        p_Photo,
        p_LocalAddress,
        p_Username,
        p_Password,
        p_GenderId,
        p_TypeId,
        p_DistrictId
    );
    COMMIT;
END;

PROCEDURE updatePerson(
    p_PersonId IN NUMBER,
    p_IdCard IN NUMBER,
    p_Name IN VARCHAR2,
    p_FirstLastName IN VARCHAR2,
    p_SecondLastName IN VARCHAR2,
    p_Birthdate IN DATE,
    p_Photo IN VARCHAR2,
    p_LocalAddress IN VARCHAR2,
    p_GenderId IN NUMBER,
    p_TypeId IN NUMBER,
    p_DistrictId IN NUMBER) IS
BEGIN
    UPDATE Person
    SET IdCard = p_IdCard,
        "Name" = p_Name,
        FirstLastName = p_FirstLastName,
        SecondLastName = p_SecondLastName,
        Birthdate = p_Birthdate,
        Photo = p_Photo,
        LocalAddress = p_LocalAddress,
        GenderId = p_GenderId,
        TypeId = p_TypeId,
        DistrictId = p_DistrictId
    WHERE PersonId = p_PersonId;
    COMMIT;
END;

PROCEDURE deletePerson(p_PersonId IN NUMBER) IS
BEGIN
    DELETE FROM Person WHERE PersonId = p_PersonId;
    COMMIT;
END;

PROCEDURE getPersons(p_PersonId IN NUMBER, pRecordSet OUT SYS_REFCURSOR) IS
BEGIN
    OPEN pRecordSet FOR
        SELECT PersonId,
               IdCard,
               "Name",
               FirstLastName,
               SecondLastName,
               Birthdate,
               Photo,
               LocalAddress,
               Username,
               GenderId,
               TypeId,
               DistrictId
        FROM Person
        WHERE PersonId = NVL(p_PersonId, PersonId);
END;

PROCEDURE loginUser(p_Username IN VARCHAR2, p_Password IN VARCHAR2, pRecordSet OUT SYS_REFCURSOR) IS
BEGIN
    OPEN pRecordSet FOR
        SELECT PersonId,
               IdCard,
               "Name",
               FirstLastName,
               SecondLastName,
               Birthdate,
               Photo,
               LocalAddress,
               Username,
               GenderId,
               TypeId,
               DistrictId
        FROM Person
        WHERE Username = p_Username AND "Password" = p_Password;
END;
END;

CREATE OR REPLACE PACKAGE genderManagement IS
PROCEDURE createGender(p_GenderName IN VARCHAR2);
PROCEDURE updateGender(p_GenderId IN NUMBER, p_GenderName IN VARCHAR2);
PROCEDURE deleteGender(p_GenderId IN NUMBER);
PROCEDURE getGenders(p_GenderId IN NUMBER, pRecordSet OUT SYS_REFCURSOR);
END;

CREATE OR REPLACE PACKAGE BODY genderManagement AS
PROCEDURE createGender(p_GenderName IN VARCHAR2) IS
BEGIN
    INSERT INTO Gender(GenderId, GenderName)
    VALUES (s_gender.nextval, p_GenderName);
    COMMIT;
END;
PROCEDURE updateGender(p_GenderId IN NUMBER, p_GenderName IN VARCHAR2) IS
BEGIN
    UPDATE Gender
    SET GenderName = p_GenderName
    WHERE GenderId = p_GenderId;
    COMMIT;
END;
PROCEDURE deleteGender(p_GenderId IN NUMBER) IS
BEGIN
    DELETE FROM Gender WHERE GenderId = p_GenderId;
END;
PROCEDURE getGenders(p_GenderId IN NUMBER, pRecordSet OUT SYS_REFCURSOR) IS
BEGIN
    OPEN pRecordSet FOR
        SELECT GenderId,
               GenderName
        FROM Gender
        WHERE GenderId = NVL(p_GenderId, GenderId);
END;
END;

CREATE OR REPLACE PACKAGE authorManagement IS
PROCEDURE createAuthor(p_PersonId IN NUMBER, p_AuthorTypeId IN NUMBER);
PROCEDURE updateAuthor(p_AuthorId IN NUMBER, p_AuthorTypeId IN NUMBER);
PROCEDURE deleteAuthor(p_AuthorId IN NUMBER);
PROCEDURE getAuthors(p_AuthorId IN NUMBER, p_AuthorTypeId IN NUMBER, pRecordSet OUT SYS_REFCURSOR);
END;

CREATE OR REPLACE PACKAGE BODY authorManagement AS
PROCEDURE createAuthor(p_PersonId IN NUMBER, p_AuthorTypeId IN NUMBER) IS
BEGIN
    INSERT INTO Author(AuthorId, AuthorTypeId)
    VALUES(p_PersonId, p_AuthorTypeId);
    COMMIT;
END;
PROCEDURE updateAuthor(p_AuthorId IN NUMBER, p_AuthorTypeId IN NUMBER) IS
BEGIN
    UPDATE Author
    SET AuthorTypeId = p_AuthorTypeId
    WHERE AuthorId = p_AuthorId;
    COMMIT;
END;
PROCEDURE deleteAuthor(p_AuthorId IN NUMBER) IS
BEGIN
    DELETE FROM Author WHERE AuthorId = p_AuthorId;
END;
PROCEDURE getAuthors(p_AuthorId IN NUMBER, p_AuthorTypeId IN NUMBER, pRecordSet OUT SYS_REFCURSOR) IS
BEGIN
    OPEN pRecordSet FOR
        SELECT AuthorId,
               AuthorTypeId
        FROM Author
        WHERE AuthorId = NVL(p_AuthorId, AuthorId);
END;
END;
