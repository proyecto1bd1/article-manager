CREATE SEQUENCE S_COUNTRY
            START WITH 0
            INCREMENT BY 1
            MINVALUE 0
            MAXVALUE 10000000
            NOCACHE
            NOCYCLE; 
        
CREATE SEQUENCE S_PROVINCE
            START WITH 0
            INCREMENT BY 1
            MINVALUE 0
            MAXVALUE 10000000
            NOCACHE
            NOCYCLE; 
        
CREATE SEQUENCE S_CANTON
            START WITH 0
            INCREMENT BY 1
            MINVALUE 0
            MAXVALUE 10000000
            NOCACHE
            NOCYCLE; 
        
CREATE SEQUENCE S_DISTRICT
            START WITH 0
            INCREMENT BY 1
            MINVALUE 0
            MAXVALUE 10000000
            NOCACHE
            NOCYCLE; 
        
CREATE SEQUENCE S_IDTYPE
            START WITH 0
            INCREMENT BY 1
            MINVALUE 0
            MAXVALUE 10000000
            NOCACHE
            NOCYCLE; 
        
CREATE SEQUENCE S_GENDER
            START WITH 0
            INCREMENT BY 1
            MINVALUE 0
            MAXVALUE 10000000
            NOCACHE
            NOCYCLE; 
        
CREATE SEQUENCE S_PERSON
            START WITH 0
            INCREMENT BY 1
            MINVALUE 0
            MAXVALUE 10000000
            NOCACHE
            NOCYCLE; 
        
CREATE SEQUENCE S_TELEPHONE
            START WITH 0
            INCREMENT BY 1
            MINVALUE 0
            MAXVALUE 10000000
            NOCACHE
            NOCYCLE; 
        
CREATE SEQUENCE S_EMAIL
            START WITH 0
            INCREMENT BY 1
            MINVALUE 0
            MAXVALUE 10000000
            NOCACHE
            NOCYCLE; 
        
CREATE SEQUENCE S_UNIVERSITY
            START WITH 0
            INCREMENT BY 1
            MINVALUE 0
            MAXVALUE 10000000
            NOCACHE
            NOCYCLE; 
        
CREATE SEQUENCE S_PARAMETER
            START WITH 0
            INCREMENT BY 1
            MINVALUE 0
            MAXVALUE 10000000
            NOCACHE
            NOCYCLE; 
        
CREATE SEQUENCE S_DIGITALNEWSPAPER
            START WITH 0
            INCREMENT BY 1
            MINVALUE 0
            MAXVALUE 10000000
            NOCACHE
            NOCYCLE; 
        
CREATE SEQUENCE S_COMMITTEE
            START WITH 0
            INCREMENT BY 1
            MINVALUE 0
            MAXVALUE 10000000
            NOCACHE
            NOCYCLE; 
        
CREATE SEQUENCE S_STORE
            START WITH 0
            INCREMENT BY 1
            MINVALUE 0
            MAXVALUE 10000000
            NOCACHE
            NOCYCLE; 
        
CREATE SEQUENCE S_PRODUCT
            START WITH 0
            INCREMENT BY 1
            MINVALUE 0
            MAXVALUE 10000000
            NOCACHE
            NOCYCLE; 
        
CREATE SEQUENCE S_PURCHASE
            START WITH 0
            INCREMENT BY 1
            MINVALUE 0
            MAXVALUE 10000000
            NOCACHE
            NOCYCLE; 
        
CREATE SEQUENCE S_PURCHASEXPRODUCT
            START WITH 0
            INCREMENT BY 1
            MINVALUE 0
            MAXVALUE 10000000
            NOCACHE
            NOCYCLE; 
        
CREATE SEQUENCE S_PURCHASEXNEWSPAPER
            START WITH 0
            INCREMENT BY 1
            MINVALUE 0
            MAXVALUE 10000000
            NOCACHE
            NOCYCLE; 
        
CREATE SEQUENCE S_PERSONXCOMMITTEE
            START WITH 0
            INCREMENT BY 1
            MINVALUE 0
            MAXVALUE 10000000
            NOCACHE
            NOCYCLE; 
        
CREATE SEQUENCE S_AUTHOR
            START WITH 0
            INCREMENT BY 1
            MINVALUE 0
            MAXVALUE 10000000
            NOCACHE
            NOCYCLE; 
        
CREATE SEQUENCE S_AUTHORTYPE
            START WITH 0
            INCREMENT BY 1
            MINVALUE 0
            MAXVALUE 10000000
            NOCACHE
            NOCYCLE; 
        
CREATE SEQUENCE S_NEWSARTICLE
            START WITH 0
            INCREMENT BY 1
            MINVALUE 0
            MAXVALUE 10000000
            NOCACHE
            NOCYCLE; 
        
CREATE SEQUENCE S_NEWSARTICLETYPE
            START WITH 0
            INCREMENT BY 1
            MINVALUE 0
            MAXVALUE 10000000
            NOCACHE
            NOCYCLE;

-- New sequences
            
CREATE SEQUENCE S_LOG
            START WITH 0
            INCREMENT BY 1
            MINVALUE 0
            MAXVALUE 10000000
            NOCACHE
            NOCYCLE


CREATE SEQUENCE S_ADMIN
            START WITH 0
            INCREMENT BY 1
            MINVALUE 0
            MAXVALUE 10000000
            NOCACHE
            NOCYCLE

