CREATE TABLE Country (
    CountryId NUMBER(10),
    CountryName VARCHAR2(25) CONSTRAINT countryName_nn NOT NULL,
                             CONSTRAINT countryName_uk UNIQUE(CountryName),
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

COMMENT ON TABLE Country IS 'Represents a Country';
COMMENT ON COLUMN Country.CountryId IS 'Internal Id of the country';
COMMENT ON COLUMN Country.CountryName IS 'Name of the country';
COMMENT ON COLUMN Country.CreationUser IS 'User who created the country';
COMMENT ON COLUMN Country.CreationDate IS 'Date the field was created';
COMMENT ON COLUMN Country.LastModifiedUser IS 'User who modified the last time';
COMMENT ON COLUMN Country.LastModifiedDate IS 'Date the field was update last time';

CREATE TABLE Province (
    ProvinceId NUMBER(10),
    ProvinceName VARCHAR2(25) CONSTRAINT provinceName_nn NOT NULL,
                              CONSTRAINT provinceName_uk UNIQUE(ProvinceName),
    CountryId NUMBER(10),
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

COMMENT ON TABLE Province IS 'Represents a Province';
COMMENT ON COLUMN Province.ProvinceId IS 'Internal Id of the province';
COMMENT ON COLUMN Province.ProvinceName IS 'Name of the province';
COMMENT ON COLUMN Province.CreationUser IS 'User who created the country';
COMMENT ON COLUMN Province.CreationDate IS 'Date the field was created';
COMMENT ON COLUMN Province.LastModifiedUser IS 'User who modified the last time';
COMMENT ON COLUMN Province.LastModifiedDate IS 'Date the field was update last time';

CREATE TABLE Canton (
    CantonId NUMBER(10),
    CantonName VARCHAR2(25) CONSTRAINT cantonName_nn NOT NULL,
    ProvinceId NUMBER(10),
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

COMMENT ON TABLE Canton IS 'Represents a Canton';
COMMENT ON COLUMN Canton.CantonId IS 'Internal Id of the Canton';
COMMENT ON COLUMN Canton.CantonName IS 'Name of the Canton';
COMMENT ON COLUMN Canton.CreationUser IS 'User who created the country';
COMMENT ON COLUMN Canton.CreationDate IS 'Date the field was created';
COMMENT ON COLUMN Canton.LastModifiedUser IS 'User who modified the last time';
COMMENT ON COLUMN Canton.LastModifiedDate IS 'Date the field was update last time';

CREATE TABLE District (
    DistrictId NUMBER(10),
    DistrictName VARCHAR2(25) CONSTRAINT districtName_nn NOT NULL,
    CantonId NUMBER(10),
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

COMMENT ON TABLE District IS 'Represents a District';
COMMENT ON COLUMN District.DistrictId IS 'Internal Id of the District';
COMMENT ON COLUMN District.DistrictName IS 'Name of the District';
COMMENT ON COLUMN District.CreationUser IS 'User who created the District';
COMMENT ON COLUMN District.CreationDate IS 'Date the field was created';
COMMENT ON COLUMN District.LastModifiedUser IS 'User who modified the last time';
COMMENT ON COLUMN District.LastModifiedDate IS 'Date the field was update last time';

CREATE TABLE University (
    UniversityId NUMBER(10),
    UniversityName VARCHAR2(30) CONSTRAINT universityname NOT NULL,
    LocalAddress VARCHAR2(30) CONSTRAINT uaddress_nn NOT NULL,
    DistrictId NUMBER(10) CONSTRAINT udistrict_nn NOT NULL,
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

COMMENT ON TABLE University IS 'Represents an University';
COMMENT ON COLUMN University.UniversityId IS 'Internal Id of the University';
COMMENT ON COLUMN University.UniversityName IS 'Name of the University';
COMMENT ON COLUMN University.LocalAddress IS 'Specific location of the University';
COMMENT ON COLUMN University.CreationUser IS 'User who created the University';
COMMENT ON COLUMN University.CreationDate IS 'Date the field was created';
COMMENT ON COLUMN University.LastModifiedUser IS 'User who modified the last time';
COMMENT ON COLUMN University.LastModifiedDate IS 'Date the field was update last time';
    
CREATE TABLE Author (
    AuthorId NUMBER(10) CONSTRAINT authorpersonid_nn NOT NULL,
    AuthorTypeId NUMBER(10) CONSTRAINT fkauthorTypeid_nn NOT NULL,
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

COMMENT ON TABLE Author IS 'Represents an Author';
COMMENT ON COLUMN Author.AuthorId IS 'Internal Id of the Author';
COMMENT ON COLUMN Author.AuthorTypeId IS 'Type of Author';
COMMENT ON COLUMN Author.CreationUser IS 'User who created the Author';
COMMENT ON COLUMN Author.CreationDate IS 'Date the field was created';
COMMENT ON COLUMN Author.LastModifiedUser IS 'User who modified the last time';
COMMENT ON COLUMN Author.LastModifiedDate IS 'Date the field was update last time';

CREATE TABLE Gender(
    GenderId NUMBER(10),
    GenderName VARCHAR2(30),
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

COMMENT ON TABLE Gender IS 'Represents a Gender';
COMMENT ON COLUMN Gender.GenderId IS 'Internal Id of the Gender';
COMMENT ON COLUMN Gender.GenderName IS 'Name of the Gender';
COMMENT ON COLUMN Gender.CreationUser IS 'User who created the Gender';
COMMENT ON COLUMN Gender.CreationDate IS 'Date the field was created';
COMMENT ON COLUMN Gender.LastModifiedUser IS 'User who modified the last time';
COMMENT ON COLUMN Gender.LastModifiedDate IS 'Date the field was update last time';

CREATE TABLE Person(
    PersonId NUMBER(10),
    IdCard NUMBER(10) CONSTRAINT idcard_nn NOT NULL,
                      CONSTRAINT idcard_uk UNIQUE(IdCard),
    "Name" VARCHAR2(20) CONSTRAINT name_nn NOT NULL,
    FirstLastName VARCHAR2(20) CONSTRAINT lastname_nn NOT NULL,
    SecondLastName VARCHAR2(20),
    Birthdate DATE CONSTRAINT birthdate_nn NOT NULL,
    Photo VARCHAR2(30),
    LocalAddress VARCHAR2(60),
    Username VARCHAR2(20) CONSTRAINT username_nn NOT NULL,
                          CONSTRAINT username_uk UNIQUE(Username),
    "Password" VARCHAR2(20) CONSTRAINT password_nn NOT NULL,
    GenderId NUMBER(10) CONSTRAINT genderid_nn NOT NULL,
    TypeId NUMBER(10) CONSTRAINT idtype_nn NOT NULL,
    DistrictId NUMBER(10) CONSTRAINT pdistrict_nn NOT NULL,
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

COMMENT ON TABLE Person IS 'Represents a Person';
COMMENT ON COLUMN Person.PersonId IS 'Internal id of the person';
COMMENT ON COLUMN Person.IdCard IS 'Unique person identifier in the real world';
COMMENT ON COLUMN Person."Name" IS 'Person first and second name representation';
COMMENT ON COLUMN Person.FirstLastName IS 'First last name of the person';
COMMENT ON COLUMN Person.SecondLastName IS 'Second last name of the person';
COMMENT ON COLUMN Person.Birthdate IS 'Date of birth of the person';
COMMENT ON COLUMN Person.Photo IS 'Picture of the person';
COMMENT ON COLUMN Person.LocalAddress IS 'Exact location of the person personal life place';
COMMENT ON COLUMN Person.Username IS 'The user name of the person';
COMMENT ON COLUMN Person.Password IS 'The password user account';
COMMENT ON COLUMN Person.GenderId IS 'The type of gender of the person';
COMMENT ON COLUMN Person.TypeId IS 'The type of identification document';
COMMENT ON COLUMN Person.DistrictId IS 'District where the person live';
COMMENT ON COLUMN Person.CreationUser IS 'User who created the person';
COMMENT ON COLUMN Person.CreationDate IS 'Date the field was created';
COMMENT ON COLUMN Person.LastModifiedUser IS 'User who modified the last time';
COMMENT ON COLUMN Person.LastModifiedDate IS 'Date the field was update last time';

CREATE TABLE Purchase(
    PurchaseId NUMBER(10),
    IdCard NUMBER(10) CONSTRAINT purchasePersonId_nn NOT NULL,
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

CREATE TABLE PurchaseXProduct (
    PurchaseId NUMBER(10),
    ProductId NUMBER(10),
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

CREATE TABLE PersonXNewsPaper (
    NewsPaperId NUMBER(10),
    IdCard NUMBER(10),
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);


CREATE TABLE PersonXCommittee (
    IdCard NUMBER(10),
    CommitteeId NUMBER(10),
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

CREATE TABLE AuthorType (
    AuthorTypeId NUMBER(10),
    Description VARCHAR2(30) CONSTRAINT authorTypeDescript_nn NOT NULL,
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);


CREATE TABLE NewsArticleType (
    TypeId NUMBER(10),
    Description VARCHAR2(20) CONSTRAINT articletypedescript_nn NOT NULL,
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

CREATE TABLE LOG (
    LogId NUMBER(10),
    FIELDNAME VARCHAR2(30),
    TABLENAME VARCHAR2(30),
    CURRENTVALUE VARCHAR2(2000),
    PREVIOUSVALUE VARCHAR2(2000),
    ArticleId NUMBER(10),
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

CREATE TABLE PxReviewsXArticle (
    IdCard NUMBER(10),
    ArticleId NUMBER(10),
    NSTARS NUMBER(10) CONSTRAINT nstars_nn NOT NULL,
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);


CREATE TABLE PXFavoriteXArticle (
    IdCard NUMBER(10),
    ArticleId NUMBER(10),
    Deleted BOOLEAN,
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);


ALTER TABLE purchase
ADD CONSTRAINT pk_purchase PRIMARY KEY (purchaseId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);

ALTER TABLE purchase
ADD CONSTRAINT fk_purchase_person FOREIGN KEY (IdCard) 
REFERENCES person(IdCard);


ALTER TABLE PurchaseXProduct
ADD CONSTRAINT pk_purchasexproduct PRIMARY KEY (purchaseId, productId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);

ALTER TABLE PurchaseXProduct
ADD CONSTRAINT fk_purchasexproduct_purchase FOREIGN KEY (purchaseId) 
REFERENCES purchase(purchaseId);

ALTER TABLE PurchaseXProduct
ADD CONSTRAINT fk_purchasexproduct_product FOREIGN KEY (productId) 
REFERENCES product(productId);


ALTER TABLE PersonXNewsPaper
ADD CONSTRAINT pk_personxnewspaper PRIMARY KEY (newspaperId, idCard)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);

ALTER TABLE PersonXNewsPaper
ADD CONSTRAINT fk_personxnewspaper_digitalnewspaper FOREIGN KEY (newspaperId) 
REFERENCES digitalNewspaper(newspaperId);

ALTER TABLE PersonXNewsPaper
ADD CONSTRAINT fk_personxnewspaper_person FOREIGN KEY (idCard) 
REFERENCES person(idCard);


ALTER TABLE PersonXCommittee
ADD CONSTRAINT pk_personxcommittee PRIMARY KEY (idCard, committeeId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);

ALTER TABLE PersonXCommittee
ADD CONSTRAINT fk_personxcommittee_person FOREIGN KEY (idCard) 
REFERENCES person(idCard);

ALTER TABLE PersonXCommittee
ADD CONSTRAINT fk_personxcommittee_committee FOREIGN KEY (committeeId) 
REFERENCES committee(committeeId);


ALTER TABLE AuthorType
ADD CONSTRAINT pk_authortype PRIMARY KEY (authorTypeId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);


ALTER TABLE NewsArticleType
ADD CONSTRAINT pk_newsarticletype PRIMARY KEY (TypeId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);


ALTER TABLE Log
ADD CONSTRAINT pk_log PRIMARY KEY (logId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);

ALTER TABLE Log
ADD CONSTRAINT fk_log_newsarticle FOREIGN KEY (articleId) 
REFERENCES newsarticle(articleId);


ALTER TABLE PXReviewsXArticle
ADD CONSTRAINT pk_pxreviewsxarticle PRIMARY KEY (idCard, articleId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);

ALTER TABLE PXReviewsXArticle
ADD CONSTRAINT fk_pxreviewsxarticle_person FOREIGN KEY (idCard) 
REFERENCES person(idCard);

ALTER TABLE PXReviewsXArticle
ADD CONSTRAINT fk_pxreviewsxarticle_newsarticle FOREIGN KEY (articleId) 
REFERENCES newsarticle(articleId);


ALTER TABLE PXFavoriteXArticle
ADD CONSTRAINT pk_pxfavoritexarticle PRIMARY KEY (idCard, articleId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);

ALTER TABLE PXFavoriteXArticle
ADD CONSTRAINT fk_pxfavoritexarticle_person FOREIGN KEY (idCard) 
REFERENCES person(idCard);

ALTER TABLE PXFavoriteXArticle
ADD CONSTRAINT fk_pxfavoritexarticle_newsarticle FOREIGN KEY (articleId) 
REFERENCES newsarticle(articleId);

ALTER TABLE Country
ADD CONSTRAINT pk_country PRIMARY KEY (CountryId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);

ALTER TABLE Province
ADD CONSTRAINT pk_province PRIMARY KEY (ProvinceId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);

ALTER TABLE Province
ADD CONSTRAINT fk_province_country FOREIGN KEY 
(CountryId) REFERENCES Country(CountryId);

ALTER TABLE Canton
ADD CONSTRAINT pk_canton PRIMARY KEY (CantonId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);

ALTER TABLE Canton
ADD CONSTRAINT fk_canton_province FOREIGN KEY 
(ProvinceID) REFERENCES Province(ProvinceId);

ALTER TABLE District
ADD CONSTRAINT pk_district PRIMARY KEY (DistrictId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);

ALTER TABLE District
ADD CONSTRAINT fk_district_canton FOREIGN KEY 
(CantonId) REFERENCES Canton(CantonId);

ALTER TABLE University
ADD CONSTRAINT pk_UNIVERSITY PRIMARY KEY (UniversityId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);

ALTER TABLE Author 
ADD CONSTRAINT pk_author PRIMARY KEY (AuthorId) 
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);
    
ALTER TABLE Author
ADD CONSTRAINT fk_author_person FOREIGN KEY 
(AuthorId) REFERENCES Person(PersonId);

-- Waiting for author type
ALTER TABLE Author
ADD CONSTRAINT fk_author_type FOREIGN KEY 
(AuthorTypeId) REFERENCES AuthorType(AuthorTypeId);

ALTER TABLE Gender
ADD CONSTRAINT pk_gender PRIMARY KEY (GenderId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);

ALTER TABLE Person
ADD CONSTRAINT pk_person PRIMARY KEY (PersonId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);

ALTER TABLE Person
ADD CONSTRAINT fk_person_gender FOREIGN KEY 
(GenderId) REFERENCES Gender(GenderId);

-- Waiting for IdType table
ALTER TABLE Person
ADD CONSTRAINT fk_person_idtype FOREIGN KEY 
(TypeId) REFERENCES IdType(TypeId);

ALTER TABLE Person
ADD CONSTRAINT fk_person_district FOREIGN KEY 
(DistrictId) REFERENCES District(DistrictId);
