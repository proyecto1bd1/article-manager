CREATE TABLE Admin(
    AdminId NUMBER(10),
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

COMMENT ON TABLE Admin IS 'Represents a Admin';
COMMENT ON COLUMN Admin.AdminId IS 'Internal Id of the admin';
COMMENT ON COLUMN Admin.CreationUser IS 'User who created the admin';
COMMENT ON COLUMN Admin.CreationDate IS 'Date the field was created';
COMMENT ON COLUMN Admin.LastModifiedUser IS 'User who modified the last time';
COMMENT ON COLUMN Admin.LastModifiedDate IS 'Date the field was update last time';

CREATE TABLE Telephone(
    TelephoneId NUMBER(10),
    PhoneNumber VARCHAR2(20) CONSTRAINT phoneNumber_nn NOT NULL,
                             CONSTRAINT phoneNumber_uk UNIQUE(PhoneNumber),
    PersonId NUMBER(10) CONSTRAINT telephonePerson_nn NOT NULL,
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE  
);

COMMENT ON TABLE Telephone IS 'Represents a Telephone';
COMMENT ON COLUMN Telephone.TelephoneId IS 'Internal Id of the telephone';
COMMENT ON COLUMN Telephone.PhoneNumber IS 'Telephone number of a person';
COMMENT ON COLUMN Telephone.CreationUser IS 'User who created the telephone';
COMMENT ON COLUMN Telephone.CreationDate IS 'Date the field was created';
COMMENT ON COLUMN Telephone.LastModifiedUser IS 'User who modified the last time';
COMMENT ON COLUMN Telephone.LastModifiedDate IS 'Date the field was update last time';

CREATE TABLE Email(
    EmailId NUMBER(10),
    EmailAddress VARCHAR2(20) CONSTRAINT emailAddress_nn NOT NULL,
    PersonId NUMBER(10) CONSTRAINT emailPerson_nn NOT NULL,
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

COMMENT ON TABLE Email IS 'Represents a Email';
COMMENT ON COLUMN Email.EmailAddress IS 'Mail Address of a person';
COMMENT ON COLUMN Email.CreationUser IS 'User who created the email';
COMMENT ON COLUMN Email.CreationDate IS 'Date the field was created';
COMMENT ON COLUMN Email.LastModifiedUser IS 'User who modified the last time';
COMMENT ON COLUMN Email.LastModifiedDate IS 'Date the field was update last time';

CREATE TABLE Parameter(
    ParameterId NUMBER(10),
    Name VARCHAR2(20) CONSTRAINT parameterName_nn NOT NULL,
    Description VARCHAR2(20) CONSTRAINT parameterDescription_nn NOT NULL,
    Value VARCHAR2(20) CONSTRAINT parameterValue_nn NOT NULL,
    UniversityId NUMBER(10) CONSTRAINT parameterUniversityId_nn NOT NULL,
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

COMMENT ON TABLE Parameter IS 'Represents a Parameter';
COMMENT ON COLUMN Parameter.ParameterId IS 'Internal Id of the parameter';
COMMENT ON COLUMN Parameter.Name IS 'Name of the Parameter';
COMMENT ON COLUMN Parameter.Description IS 'Description of the parameter';
COMMENT ON COLUMN Parameter.Value IS 'Value of the parameter';
COMMENT ON COLUMN Parameter.CreationUser IS 'User who created the parameter';
COMMENT ON COLUMN Parameter.CreationDate IS 'Date the field was created';
COMMENT ON COLUMN Parameter.LastModifiedUser IS 'User who modified the last time';
COMMENT ON COLUMN Parameter.LastModifiedDate IS 'Date the field was update last time';

CREATE TABLE DigitalNewsPaper(
    NewsPaperId NUMBER(10),
    NewsPaperName VARCHAR2(30) CONSTRAINT newsPaperName_nn NOT NULL,
    CommitteeId NUMBER(10) CONSTRAINT newsPaperCommiteeId_nn NOT NULL,
    UniversityId NUMBER(10) CONSTRAINT newsPaperUniversityId_nn NOT NULL,
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

COMMENT ON TABLE DigitalNewsPaper IS 'Represents a DigitalNewsPaper';
COMMENT ON COLUMN DigitalNewsPaper.NewsPaperId IS 'Internal Id of the digitalNewsPaper';
COMMENT ON COLUMN DigitalNewsPaper.NewsPaperName IS 'Name of the digitalNewsPaper';
COMMENT ON COLUMN DigitalNewsPaper.CreationUser IS 'User who created the digitalNewsPaper';
COMMENT ON COLUMN DigitalNewsPaper.CreationDate IS 'Date the field was created';
COMMENT ON COLUMN DigitalNewsPaper.LastModifiedUser IS 'User who modified the last time';
COMMENT ON COLUMN DigitalNewsPaper.LastModifiedDate IS 'Date the field was update last time';

CREATE TABLE Store (
    StoreId NUMBER(10),
    Description VARCHAR2(30) CONSTRAINT storeDescription_nn NOT NULL,
    NewsPaperId NUMBER(10) CONSTRAINT storeNewsPaperId_nn NOT NULL,
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

COMMENT ON TABLE Store IS 'Represents a DigitalNewsPaper';
COMMENT ON COLUMN Store.StoreId IS 'Internal Id of the digitalNewsPaper';
COMMENT ON COLUMN Store.Description IS 'Description of the store';
COMMENT ON COLUMN Store.CreationUser IS 'User who created the store';
COMMENT ON COLUMN Store.CreationDate IS 'Date the field was created';
COMMENT ON COLUMN Store.LastModifiedUser IS 'User who modified the last time';
COMMENT ON COLUMN Store.LastModifiedDate IS 'Date the field was update last time';

CREATE TABLE Product(
    ProductId NUMBER(10),
    Name VARCHAR2(30) CONSTRAINT productName_nn NOT NULL,
    PointsPrice NUMBER(10) CONSTRAINT productPointsPrice_nn NOT NULL,
    NumberInStock NUMBER(10) CONSTRAINT pruductNumberInStock_nn NOT NULL,
    StoreId NUMBER(10) CONSTRAINT productStoreId_nn NOT NULL,
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

COMMENT ON TABLE Product IS 'Represents a Product';
COMMENT ON COLUMN Product.ProductId IS 'Internal Id of the product';
COMMENT ON COLUMN Product.Name IS 'Name of the Product';
COMMENT ON COLUMN Product.PointsPrice IS 'Points you need to acquire a product';
COMMENT ON COLUMN Product.CreationUser IS 'User who created the store';
COMMENT ON COLUMN Product.CreationDate IS 'Date the field was created';
COMMENT ON COLUMN Product.LastModifiedUser IS 'User who modified the last time';
COMMENT ON COLUMN Product.LastModifiedDate IS 'Date the field was update last time';

CREATE TABLE Committee(
    CommitteeId NUMBER(10),
    Description VARCHAR2(20) CONSTRAINT committeeDescription_nn NOT NULL,
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

COMMENT ON TABLE Committee IS 'Represents a Committee';
COMMENT ON COLUMN Committee.CommitteeId IS 'Internal Id of the Committee';
COMMENT ON COLUMN Committee.Description IS 'Description of the Committee';
COMMENT ON COLUMN Committee.CreationUser IS 'User who created the store';
COMMENT ON COLUMN Committee.CreationDate IS 'Date the field was created';
COMMENT ON COLUMN Committee.LastModifiedUser IS 'User who modified the last time';
COMMENT ON COLUMN Committee.LastModifiedDate IS 'Date the field was update last time';

CREATE TABLE PXCommentXArticle (
    PersonId NUMBER(10),
    ArticleId NUMBER(10),
    Comments VARCHAR2(30) CONSTRAINT pXCommentXArticleComments_nn NOT NULL,
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

COMMENT ON TABLE PXCommentXArticle IS 'Represents a PXCommentXArticle';
COMMENT ON COLUMN PXCommentXArticle.Comments IS 'Comment of an article';
COMMENT ON COLUMN PXCommentXArticle.CreationUser IS 'User who created the PXCommentXArticle';
COMMENT ON COLUMN PXCommentXArticle.CreationDate IS 'Date the field was created';
COMMENT ON COLUMN PXCommentXArticle.LastModifiedUser IS 'User who modified the last time';
COMMENT ON COLUMN PXCommentXArticle.LastModifiedDate IS 'Date the field was update last time';

CREATE TABLE Purchase(
    PurchaseId NUMBER(10),
    PersonId NUMBER(10) CONSTRAINT purchasePersonId_nn NOT NULL,
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

CREATE TABLE PurchaseXProduct (
    PurchaseId NUMBER(10),
    ProductId NUMBER(10),
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

CREATE TABLE PersonXNewsPaper (
    NewsPaperId NUMBER(10),
    PersonId NUMBER(10),
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

CREATE TABLE PersonXCommittee (
    PersonId NUMBER(10),
    CommitteeId NUMBER(10),
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

CREATE TABLE AuthorType (
    AuthorTypeId NUMBER(10),
    Description VARCHAR2(30) CONSTRAINT authorTypeDescript_nn NOT NULL,
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);


CREATE TABLE NewsArticleType (
    TypeId NUMBER(10),
    Description VARCHAR2(20) CONSTRAINT articletypedescript_nn NOT NULL,
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

CREATE TABLE Log (
    LogId NUMBER(10),
    FIELDNAME VARCHAR2(30),
    TABLENAME VARCHAR2(30),
    CURRENTVALUE VARCHAR2(2000),
    PREVIOUSVALUE VARCHAR2(2000),
    ArticleId NUMBER(10),
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

CREATE TABLE PxReviewsXArticle (
    PersonId NUMBER(10),
    ArticleId NUMBER(10),
    NSTARS NUMBER(10) CONSTRAINT nstars_nn NOT NULL,
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

CREATE TABLE PXFavoriteXArticle (
    PersonId NUMBER(10),
    ArticleId NUMBER(10),
    Deleted NUMBER(10),
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

CREATE TABLE Country (
    CountryId NUMBER(10),
    CountryName VARCHAR2(25) CONSTRAINT countryName_nn NOT NULL,
                             CONSTRAINT countryName_uk UNIQUE(CountryName),
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

COMMENT ON TABLE Country IS 'Represents a Country';
COMMENT ON COLUMN Country.CountryId IS 'Internal Id of the country';
COMMENT ON COLUMN Country.CountryName IS 'Name of the country';
COMMENT ON COLUMN Country.CreationUser IS 'User who created the country';
COMMENT ON COLUMN Country.CreationDate IS 'Date the field was created';
COMMENT ON COLUMN Country.LastModifiedUser IS 'User who modified the last time';
COMMENT ON COLUMN Country.LastModifiedDate IS 'Date the field was update last time';

CREATE TABLE Province (
    ProvinceId NUMBER(10),
    ProvinceName VARCHAR2(25) CONSTRAINT provinceName_nn NOT NULL,
                              CONSTRAINT provinceName_uk UNIQUE(ProvinceName),
    CountryId NUMBER(10),
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

COMMENT ON TABLE Province IS 'Represents a Province';
COMMENT ON COLUMN Province.ProvinceId IS 'Internal Id of the province';
COMMENT ON COLUMN Province.ProvinceName IS 'Name of the province';
COMMENT ON COLUMN Province.CreationUser IS 'User who created the country';
COMMENT ON COLUMN Province.CreationDate IS 'Date the field was created';
COMMENT ON COLUMN Province.LastModifiedUser IS 'User who modified the last time';
COMMENT ON COLUMN Province.LastModifiedDate IS 'Date the field was update last time';

CREATE TABLE Canton (
    CantonId NUMBER(10),
    CantonName VARCHAR2(25) CONSTRAINT cantonName_nn NOT NULL,
    ProvinceId NUMBER(10),
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

COMMENT ON TABLE Canton IS 'Represents a Canton';
COMMENT ON COLUMN Canton.CantonId IS 'Internal Id of the Canton';
COMMENT ON COLUMN Canton.CantonName IS 'Name of the Canton';
COMMENT ON COLUMN Canton.CreationUser IS 'User who created the country';
COMMENT ON COLUMN Canton.CreationDate IS 'Date the field was created';
COMMENT ON COLUMN Canton.LastModifiedUser IS 'User who modified the last time';
COMMENT ON COLUMN Canton.LastModifiedDate IS 'Date the field was update last time';

CREATE TABLE District (
    DistrictId NUMBER(10),
    DistrictName VARCHAR2(25) CONSTRAINT districtName_nn NOT NULL,
    CantonId NUMBER(10),
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

COMMENT ON TABLE District IS 'Represents a District';
COMMENT ON COLUMN District.DistrictId IS 'Internal Id of the District';
COMMENT ON COLUMN District.DistrictName IS 'Name of the District';
COMMENT ON COLUMN District.CreationUser IS 'User who created the District';
COMMENT ON COLUMN District.CreationDate IS 'Date the field was created';
COMMENT ON COLUMN District.LastModifiedUser IS 'User who modified the last time';
COMMENT ON COLUMN District.LastModifiedDate IS 'Date the field was update last time';

CREATE TABLE University (
    UniversityId NUMBER(10),
    UniversityName VARCHAR2(30) CONSTRAINT universityname NOT NULL,
    LocalAddress VARCHAR2(30) CONSTRAINT uaddress_nn NOT NULL,
    DistrictId NUMBER(10) CONSTRAINT udistrict_nn NOT NULL,
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

COMMENT ON TABLE University IS 'Represents an University';
COMMENT ON COLUMN University.UniversityId IS 'Internal Id of the University';
COMMENT ON COLUMN University.UniversityName IS 'Name of the University';
COMMENT ON COLUMN University.LocalAddress IS 'Specific location of the University';
COMMENT ON COLUMN University.CreationUser IS 'User who created the University';
COMMENT ON COLUMN University.CreationDate IS 'Date the field was created';
COMMENT ON COLUMN University.LastModifiedUser IS 'User who modified the last time';
COMMENT ON COLUMN University.LastModifiedDate IS 'Date the field was update last time';
    
CREATE TABLE Author (
    AuthorId NUMBER(10) CONSTRAINT authorpersonid_nn NOT NULL,
    AuthorTypeId NUMBER(10) CONSTRAINT fkauthorTypeid_nn NOT NULL,
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

COMMENT ON TABLE Author IS 'Represents an Author';
COMMENT ON COLUMN Author.AuthorId IS 'Internal Id of the Author';
COMMENT ON COLUMN Author.AuthorTypeId IS 'Type of Author';
COMMENT ON COLUMN Author.CreationUser IS 'User who created the Author';
COMMENT ON COLUMN Author.CreationDate IS 'Date the field was created';
COMMENT ON COLUMN Author.LastModifiedUser IS 'User who modified the last time';
COMMENT ON COLUMN Author.LastModifiedDate IS 'Date the field was update last time';

CREATE TABLE Gender(
    GenderId NUMBER(10),
    GenderName VARCHAR2(30),
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

COMMENT ON TABLE Gender IS 'Represents a Gender';
COMMENT ON COLUMN Gender.GenderId IS 'Internal Id of the Gender';
COMMENT ON COLUMN Gender.GenderName IS 'Name of the Gender';
COMMENT ON COLUMN Gender.CreationUser IS 'User who created the Gender';
COMMENT ON COLUMN Gender.CreationDate IS 'Date the field was created';
COMMENT ON COLUMN Gender.LastModifiedUser IS 'User who modified the last time';
COMMENT ON COLUMN Gender.LastModifiedDate IS 'Date the field was update last time';

CREATE TABLE NEWSARTICLE (
    ARTICLEID NUMBER(10),
    TITLE VARCHAR2(40) CONSTRAINT title_nn NOT NULL,
    TEXT VARCHAR2(2000) CONSTRAINT text_nn NOT NULL,
    PUBLICATIONDATE DATE,
    PHOTO VARCHAR2(30),
    AUTHORID NUMBER(10) CONSTRAINT newsauthor_nn NOT NULL,
    ARTICLETYPEID NUMBER(10) CONSTRAINT newstypeid NOT NULL,
    NEWSPAPERID NUMBER(10) CONSTRAINT newspaperid NOT NULL,
    COMMITTEEIDAPPROV NUMBER(10),
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);

CREATE TABLE IDTYPE(
    TypeId NUMBER(10),
    DESCRIPTION VARCHAR2(30) CONSTRAINT idtype_descript_nn NOT NULL,
    MASK VARCHAR2(30) CONSTRAINT idtype_mask_nn NOT NULL,
    CreationUser VARCHAR2(30),
    CreationDate DATE,
    LastModifiedUser VARCHAR2(30),
    LastModifiedDate DATE
);


ALTER TABLE IDTYPE
ADD CONSTRAINT pk_IDTYPE PRIMARY KEY (TypeId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);

ALTER TABLE NEWSARTICLE 
    ADD CONSTRAINT pk_NEWSARTICLE PRIMARY KEY (ARTICLEID) 
    USING INDEX
    TABLESPACE P1DB_IND PCTFREE 20
    STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);


ALTER TABLE Country
ADD CONSTRAINT pk_country PRIMARY KEY (CountryId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);

ALTER TABLE Province
ADD CONSTRAINT pk_province PRIMARY KEY (ProvinceId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);

ALTER TABLE Canton
ADD CONSTRAINT pk_canton PRIMARY KEY (CantonId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);


ALTER TABLE District
ADD CONSTRAINT pk_district PRIMARY KEY (DistrictId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);

ALTER TABLE Author 
ADD CONSTRAINT pk_author PRIMARY KEY (AuthorId) 
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);


ALTER TABLE Gender
ADD CONSTRAINT pk_gender PRIMARY KEY (GenderId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);

ALTER TABLE Person
ADD CONSTRAINT pk_person PRIMARY KEY (PersonId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);


ALTER TABLE purchase
ADD CONSTRAINT pk_purchase PRIMARY KEY (purchaseId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);


ALTER TABLE PurchaseXProduct
ADD CONSTRAINT pk_purchasexproduct PRIMARY KEY (purchaseId, productId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);


ALTER TABLE PersonXNewsPaper
ADD CONSTRAINT pk_personxnewspaper PRIMARY KEY (newspaperId, PersonId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);

ALTER TABLE PersonXCommittee
ADD CONSTRAINT pk_personxcommittee PRIMARY KEY (PersonId, committeeId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);


ALTER TABLE AuthorType
ADD CONSTRAINT pk_authortype PRIMARY KEY (authorTypeId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);


ALTER TABLE NewsArticleType
ADD CONSTRAINT pk_newsarticletype PRIMARY KEY (TypeId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);


ALTER TABLE Log
ADD CONSTRAINT pk_log PRIMARY KEY (logId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);


ALTER TABLE PXReviewsXArticle
ADD CONSTRAINT pk_pxreviewsxarticle PRIMARY KEY (PersonId, articleId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);

ALTER TABLE PXFavoriteXArticle
ADD CONSTRAINT pk_pxfavoritexarticle PRIMARY KEY (PersonId, articleId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);



ALTER TABLE Admin
ADD CONSTRAINT pk_Admin PRIMARY KEY (AdminId)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);

ALTER TABLE Telephone
ADD CONSTRAINT pk_TELEPHONE PRIMARY KEY (TELEPHONEID)
USING INDEX
TABLESPACE P1DB_IND PCTFREE 20
STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);

ALTER TABLE Email
    ADD CONSTRAINT pk_Email PRIMARY KEY (EmailId)
    USING INDEX
    TABLESPACE P1DB_IND PCTFREE 20
    STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);

ALTER TABLE University
    ADD CONSTRAINT pk_University PRIMARY KEY (UniversityId)
    USING INDEX
    TABLESPACE P1DB_IND PCTFREE 20
    STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);
    
ALTER TABLE Parameter
    ADD CONSTRAINT pk_Parameter PRIMARY KEY (ParameterId)
    USING INDEX
    TABLESPACE P1DB_IND PCTFREE 20
    STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);
    
ALTER TABLE DigitalNewsPaper
    ADD CONSTRAINT pk_DigitalNewsPaper PRIMARY KEY (NewsPaperId)
    USING INDEX
    TABLESPACE P1DB_IND PCTFREE 20
    STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);

ALTER TABLE Store
    ADD CONSTRAINT pk_Store PRIMARY KEY (StoreId)
    USING INDEX
    TABLESPACE P1DB_IND PCTFREE 20
    STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);
    
ALTER TABLE Product
    ADD CONSTRAINT pk_Product PRIMARY KEY (ProductId)
    USING INDEX
    TABLESPACE P1DB_IND PCTFREE 20
    STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);
    
ALTER TABLE Committee
    ADD CONSTRAINT pk_Committee PRIMARY KEY (CommitteeId)
    USING INDEX
    TABLESPACE  P1DB_IND PCTFREE 20
    STORAGE (INITIAL 10K NEXT 10 PCTINCREASE 0);
    
ALTER TABLE PXCommentXArticle
    ADD CONSTRAINT pk_PXCommentXArticle PRIMARY KEY (PersonId, ArticleId) 
    USING INDEX
    TABLESPACE P1DB_IND PCTFREE 20
    STORAGE (INITIAL 10K NEXT 10K PCTINCREASE 0);

--DEPENDE DE LA TABLA PERSON    
ALTER TABLE Admin
    ADD CONSTRAINT fk_admin_person FOREIGN KEY 
    (AdminId) REFERENCES Person(PersonId);
    
--DEPENDE DE LA TABLA PERSON  
ALTER TABLE Telephone
    ADD CONSTRAINT fk_telephone_person FOREIGN KEY 
    (PersonId) REFERENCES Person(PersonId);
   
--DEPENDE DE LA TABLA PERSON 
ALTER TABLE Email
    ADD CONSTRAINT fk_email_person FOREIGN KEY 
    (PersonId) REFERENCES Person(PersonId);
  
--DEPENDE DE LA TABLA DISTRICT   
ALTER TABLE University
    ADD CONSTRAINT fk_university_district FOREIGN KEY 
    (DistrictId) REFERENCES District(DistrictId);
    
   
ALTER TABLE Parameter
    ADD CONSTRAINT fk_parameter_university FOREIGN KEY 
    (UniversityId) REFERENCES University(UniversityId);
    
ALTER TABLE DigitalNewsPaper
    ADD CONSTRAINT fk_digitalnewspaper_university FOREIGN KEY 
    (UniversityId) REFERENCES University(UniversityId);
    
ALTER TABLE DigitalNewsPaper
    ADD CONSTRAINT fk_digitalnewspaper_committee FOREIGN KEY 
    (CommitteeId) REFERENCES Committee(CommitteeId);
    
ALTER TABLE Store
    ADD CONSTRAINT fk_store_digitalnewspaper FOREIGN KEY 
    (NewsPaperId) REFERENCES DigitalNewsPaper(NewsPaperId);
    
ALTER TABLE Product
    ADD CONSTRAINT fk_product_store FOREIGN KEY 
    (StoreId) REFERENCES Store(StoreId);
    
--DEPENDE DE LA TABLA PERSON  
ALTER TABLE PXCommentXArticle
    ADD CONSTRAINT fk_pxcommenxcarticle_p FOREIGN KEY 
    (PersonId) REFERENCES person(PersonId);
    
--DEPENDE DE LA TABLA NEWSARTICLE  
ALTER TABLE PXCommentXArticle
    ADD CONSTRAINT fk_pxcommentxarticle_article FOREIGN KEY 
    (ArticleId) REFERENCES NewsArticle(ArticleId);
    
ALTER TABLE PXFavoriteXArticle
ADD CONSTRAINT fk_pxfavoritexarticle_p FOREIGN KEY (PersonId) 
REFERENCES person(PersonId);

ALTER TABLE PXFavoriteXArticle
ADD CONSTRAINT fk_pxfavoritexarticle_n FOREIGN KEY (articleId) 
REFERENCES newsarticle(articleId);

ALTER TABLE PXReviewsXArticle
ADD CONSTRAINT fk_pxreviewsxarticle_p FOREIGN KEY (PersonId) 
REFERENCES person(PersonId);

ALTER TABLE PXReviewsXArticle
ADD CONSTRAINT fk_pxreviewsxarticle_n FOREIGN KEY (articleId) 
REFERENCES newsarticle(articleId);

ALTER TABLE Log
ADD CONSTRAINT fk_log_newsarticle FOREIGN KEY (articleId) 
REFERENCES newsarticle(articleId);

ALTER TABLE PersonXCommittee
ADD CONSTRAINT fk_personxcommittee_person FOREIGN KEY (PersonId) 
REFERENCES person(PersonId);

ALTER TABLE PersonXCommittee
ADD CONSTRAINT fk_personxcommittee_committee FOREIGN KEY (committeeId) 
REFERENCES committee(committeeId);

ALTER TABLE PersonXNewsPaper
ADD CONSTRAINT fk_personxnewsp_dnewspaper FOREIGN KEY (newspaperId) 
REFERENCES digitalNewspaper(newspaperId);

ALTER TABLE PersonXNewsPaper
ADD CONSTRAINT fk_personxnewspaper_person FOREIGN KEY (PersonId) 
REFERENCES person(PersonId);


ALTER TABLE PurchaseXProduct
ADD CONSTRAINT fk_purchasexproduct_purchase FOREIGN KEY (purchaseId) 
REFERENCES purchase(purchaseId);

ALTER TABLE purchase
ADD CONSTRAINT fk_purchase_person FOREIGN KEY (PersonId) 
REFERENCES person(PersonId);

-- Waiting for IdType table
ALTER TABLE Person
ADD CONSTRAINT fk_person_idtype FOREIGN KEY 
(TypeId) REFERENCES IdType(TypeId);

ALTER TABLE Person
ADD CONSTRAINT fk_person_district FOREIGN KEY 
(DistrictId) REFERENCES District(DistrictId);


ALTER TABLE PurchaseXProduct
ADD CONSTRAINT fk_purchasexproduct_product FOREIGN KEY (productId) 
REFERENCES product(productId);

ALTER TABLE Person
ADD CONSTRAINT fk_person_gender FOREIGN KEY 
(GenderId) REFERENCES Gender(GenderId);

ALTER TABLE Author
ADD CONSTRAINT fk_author_person FOREIGN KEY 
(AuthorId) REFERENCES Person(PersonId);

-- Waiting for author type
ALTER TABLE Author
ADD CONSTRAINT fk_author_type FOREIGN KEY 
(AuthorTypeId) REFERENCES AuthorType(AuthorTypeId);

ALTER TABLE District
ADD CONSTRAINT fk_district_canton FOREIGN KEY 
(CantonId) REFERENCES Canton(CantonId);

ALTER TABLE Canton
ADD CONSTRAINT fk_canton_province FOREIGN KEY 
(ProvinceID) REFERENCES Province(ProvinceId);

ALTER TABLE Province
ADD CONSTRAINT fk_province_country FOREIGN KEY 
(CountryId) REFERENCES Country(CountryId);

ALTER TABLE NEWSARTICLE 
    ADD CONSTRAINT fk_article_author  FOREIGN KEY 
    (AUTHORID) REFERENCES AUTHOR(AUTHORID);
    
ALTER TABLE NEWSARTICLE
    ADD CONSTRAINT fk_article_type FOREIGN KEY 
    (ARTICLETYPEID) REFERENCES NEWSARTICLETYPE(TYPEID);
    








