CREATE OR REPLACE PACKAGE countryManagement IS
PROCEDURE createCountry(p_CountryName IN VARCHAR2);
PROCEDURE updateCountry(p_CountryId IN NUMBER, p_CountryName IN VARCHAR2);
PROCEDURE deleteCountry(p_CountryId IN NUMBER);
PROCEDURE getCountries(p_CountryId IN NUMBER, pRecordSet OUT SYS_REFCURSOR);
END;

CREATE OR REPLACE PACKAGE BODY countryManagement AS
PROCEDURE createCountry(p_CountryName IN VARCHAR2) IS
BEGIN
    INSERT INTO Country(CountryId, CountryName)
    VALUES (s_country.nextval, p_CountryName);
    COMMIT;
END;

PROCEDURE updateCountry(p_CountryId IN NUMBER, p_CountryName IN VARCHAR2) IS
BEGIN
    UPDATE Country
    SET CountryName = p_CountryName
    WHERE CountryId = p_CountryId;
    COMMIT;
END;

PROCEDURE deleteCountry(p_CountryId IN NUMBER) IS
BEGIN
    DELETE FROM Country WHERE CountryId = p_CountryId;
    COMMIT;
END;

PROCEDURE getCountries(p_CountryId IN NUMBER, pRecordSet OUT SYS_REFCURSOR) IS
BEGIN
    OPEN pRecordSet FOR
        SELECT CountryId,
               CountryName
        FROM Country
        WHERE CountryId = NVL(p_CountryId, CountryId);
END;
END;

CREATE OR REPLACE PACKAGE provinceManagement IS
PROCEDURE createProvince(p_ProvinceName IN VARCHAR2, p_CountryId IN NUMBER);
PROCEDURE updateProvince(p_ProvinceId IN NUMBER, p_ProvinceName IN VARCHAR2, p_CountryId IN NUMBER);
PROCEDURE deleteProvince(p_ProvinceId IN NUMBER);
PROCEDURE getProvinces(p_ProvinceId IN NUMBER, p_CountryId IN NUMBER, pRecordSet OUT SYS_REFCURSOR);
END;

CREATE OR REPLACE PACKAGE BODY provinceManagement AS
PROCEDURE createProvince(p_ProvinceName IN VARCHAR2, p_CountryId IN NUMBER) IS
BEGIN
    INSERT INTO Province(ProvinceId, ProvinceName, CountryId)
    VALUES (s_province.nextval, p_ProvinceName, p_CountryId);
    COMMIT;
END;
PROCEDURE updateProvince(p_ProvinceId IN NUMBER, p_ProvinceName IN VARCHAR2, p_CountryId IN NUMBER) IS
BEGIN
    UPDATE Province
    SET ProvinceName = p_ProvinceName,
        CountryId = p_CountryId
    WHERE ProvinceId = p_ProvinceId;
    COMMIT;
END;
PROCEDURE deleteProvince(p_ProvinceId IN NUMBER) IS
BEGIN
    DELETE FROM Province WHERE ProvinceId = p_ProvinceId;
    COMMIT;
END;
PROCEDURE getProvinces(p_ProvinceId IN NUMBER, p_CountryId IN NUMBER, pRecordSet OUT SYS_REFCURSOR) IS
BEGIN
    OPEN pRecordSet FOR
        SELECT ProvinceId,
               ProvinceName,
               CountryId
        FROM Province
        WHERE ProvinceId = NVL(p_ProvinceId, ProvinceId)
        AND CountryId = NVL(p_CountryId, CountryId);
END;
END;

CREATE OR REPLACE PACKAGE cantonManagement IS
PROCEDURE createCanton(p_CantonName IN VARCHAR2, p_ProvinceId IN NUMBER);
PROCEDURE updateCanton(p_CantonId IN NUMBER, p_CantonName IN VARCHAR2, p_ProvinceId IN NUMBER);
PROCEDURE deleteCanton(p_CantonId IN NUMBER);
PROCEDURE getCantons(p_CantonId IN NUMBER, p_ProvinceId IN NUMBER, pRecordSet OUT SYS_REFCURSOR);
END;

CREATE OR REPLACE PACKAGE BODY cantonManagement AS
PROCEDURE createCanton(p_CantonName IN VARCHAR2, p_ProvinceId IN NUMBER) IS
BEGIN
    INSERT INTO Canton (CantonId, CantonName, ProvinceId)
    VALUES (s_canton.nextval, p_CantonName, p_ProvinceId);
    COMMIT;
END;
PROCEDURE updateCanton(p_CantonId IN NUMBER, p_CantonName IN VARCHAR2, p_ProvinceId IN NUMBER) IS
BEGIN
    UPDATE Canton
    SET CantonName = p_CantonName,
        ProvinceId = p_ProvinceId
    WHERE CantonId = p_CantonId;
    COMMIT;
END;
PROCEDURE deleteCanton(p_CantonId IN NUMBER) IS
BEGIN
    DELETE FROM Canton WHERE CantonId = p_CantonId;
    COMMIT;
END;
PROCEDURE getCantons(p_CantonId IN NUMBER, p_ProvinceId IN NUMBER, pRecordSet OUT SYS_REFCURSOR) IS
BEGIN
    OPEN pRecordSet FOR
        SELECT CantonId,
               CantonName,
               ProvinceId
        FROM Canton
        WHERE CantonId = NVL(p_CantonId, CantonId)
        AND ProvinceId = NVL(p_ProvinceId, ProvinceId);
END;
END;

CREATE OR REPLACE PACKAGE districtManagement IS
PROCEDURE createDistrict(p_DistrictName IN VARCHAR2, p_CantonId IN NUMBER);
PROCEDURE updateDistrict(p_DistrictId IN NUMBER, p_DistrictName IN VARCHAR2, p_CantonId IN NUMBER);
PROCEDURE deleteDistrict(p_DistrictId IN NUMBER);
PROCEDURE getDistricts(p_DistrictId IN NUMBER, p_CantonId IN NUMBER, pRecordSet OUT SYS_REFCURSOR);
END;

CREATE OR REPLACE PACKAGE BODY districtManagement AS
PROCEDURE createDistrict(p_DistrictName IN VARCHAR2, p_CantonId IN NUMBER) IS
BEGIN
    INSERT INTO District(DistrictId, DistrictName, CantonId)
    VALUES (s_district.nextval, p_DistrictName, p_CantonId);
    COMMIT;
END;
PROCEDURE updateDistrict(p_DistrictId IN NUMBER, p_DistrictName IN VARCHAR2, p_CantonId IN NUMBER) IS
BEGIN
    UPDATE District
    SET DistrictName = p_DistrictName,
        CantonId = p_CantonId
    WHERE DistrictId = p_DistrictId;
END;
PROCEDURE deleteDistrict(p_DistrictId IN NUMBER) IS
BEGIN
    DELETE FROM District WHERE DistrictId = p_DistrictId;
    COMMIT;
END;
PROCEDURE getDistricts(p_DistrictId IN NUMBER, p_CantonId IN NUMBER, pRecordSet OUT SYS_REFCURSOR) IS
BEGIN
    OPEN pRecordSet FOR
        SELECT DistrictId,
               DistrictName,
               CantonId
        FROM District
        WHERE DistrictId = NVL(p_DistrictId, DistrictId)
        AND CantonId = NVL(p_CantonId, CantonId);
END;
END;
